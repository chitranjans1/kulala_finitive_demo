package com.portal.common.domain.grid;

public class Filter {

   private String value;

   public Filter(){}

   public Filter(String value) {
       this.value = value;
   }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
