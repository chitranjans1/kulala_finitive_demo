package com.portal.common.domain.grid;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Grid<T> {

    private List<Column> columns = null;
    private List<T> rows = new ArrayList<>();

    public Grid() {
    }

    public Grid(List<Column> columns, List<T> rows) {
        this.columns = columns;
        this.rows = rows;
    }

    public long getTotalCount() {
        return rows.size();
    }
}