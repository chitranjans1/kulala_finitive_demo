package com.portal.common.domain.grid;

public class Style {

    public Style(){}

    public Style(Integer width) {
        this.width = width;
    }

    private Integer width;

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

}