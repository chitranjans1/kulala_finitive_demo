package com.portal.common.domain.grid;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Column {

    private String name;
    private String field;

    public Column() {

    }

    public Column(String name, String field) {
        this.name = name;
        this.field = field;
    }
}