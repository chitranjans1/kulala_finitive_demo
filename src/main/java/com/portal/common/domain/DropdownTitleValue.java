package com.portal.common.domain;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class DropdownTitleValue implements Serializable {
    private String title;
    private Object value;
}
