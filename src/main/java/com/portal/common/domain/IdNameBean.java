package com.portal.common.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class IdNameBean implements Serializable {
    private final Long id;
    private final String name;
}
