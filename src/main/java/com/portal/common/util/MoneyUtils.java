package com.portal.common.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;


public class MoneyUtils {
    private static final NumberFormat nf0 = NumberFormat.getInstance();
    private static final NumberFormat nf1 = NumberFormat.getInstance();
    private static final NumberFormat nf2 = NumberFormat.getInstance();
    private static final NumberFormat nf3 = NumberFormat.getInstance();

    static {
        nf0.setGroupingUsed(true);
        nf0.setMaximumFractionDigits(0);
        nf1.setGroupingUsed(true);
        nf1.setMinimumFractionDigits(1);
        nf1.setMaximumFractionDigits(1);
        nf2.setGroupingUsed(true);
        nf2.setMinimumFractionDigits(2);
        nf2.setMaximumFractionDigits(2);
        nf3.setGroupingUsed(true);
        nf3.setMinimumFractionDigits(3);
        nf3.setMaximumFractionDigits(3);
    }

    private static NumberFormat money = NumberFormat.getCurrencyInstance(Locale.US);

    public static String toMoneyString(Number n) {
        if (n == null) return "N/A";
        String s = nf0.format(n);
        return "$" + s;
    }

    public static String toMoneyString1(Number n) {
        if (n == null) return "N/A";
        String s = nf1.format(n);
        return "$" + s;
    }

    public static String toMoneyString2(Number n) {
        if (n == null) return "N/A";
        String s = nf2.format(n);
        return "$" + s;
    }

    public static String toPercentString1(Number n) {
        if (n == null) return "N/A";
        String s = nf1.format(n);
        return s + "%";
    }

    public static String toPercentString2(Number n) {
        if (n == null) return "N/A";
        String s = nf2.format(n);
        return s + "%";
    }

    public static String toPercentString3(Number n) {
        if (n == null) return "N/A";
        String s = nf3.format(n);
        return s + "%";
    }

    public static String toPercent100String1(BigDecimal n) {
        if (n == null) return "N/A";
        String s = nf1.format(n.multiply(BigDecimal.valueOf(100)));
        return s + "%";
    }

    public static String toPercent100String2(BigDecimal n) {
        if (n == null) return "N/A";
        String s = nf2.format(n.multiply(BigDecimal.valueOf(100)));
        return s + "%";
    }

    public static String toPercent100String3(BigDecimal n) {
        if (n == null) return "N/A";
        String s = nf3.format(n.multiply(BigDecimal.valueOf(100)));
        return s + "%";
    }



        private static final String[] tensNames = {
                "",
                " ten",
                " twenty",
                " thirty",
                " forty",
                " fifty",
                " sixty",
                " seventy",
                " eighty",
                " ninety"
        };

        private static final String[] numNames = {
                "",
                " one",
                " two",
                " three",
                " four",
                " five",
                " six",
                " seven",
                " eight",
                " nine",
                " ten",
                " eleven",
                " twelve",
                " thirteen",
                " fourteen",
                " fifteen",
                " sixteen",
                " seventeen",
                " eighteen",
                " nineteen"
        };


        private static String convertLessThanOneThousand(int number) {
            String soFar;

            if (number % 100 < 20){
                soFar = numNames[number % 100];
                number /= 100;
            }
            else {
                soFar = numNames[number % 10];
                number /= 10;

                soFar = tensNames[number % 10] + soFar;
                number /= 10;
            }
            if (number == 0) return soFar;
            return numNames[number] + " hundred" + soFar;
        }


        public static String convertNumberToWords(long number) {
            // 0 to 999 999 999 999
            if (number == 0) { return "zero"; }

            String snumber = Long.toString(number);

            // pad with "0"
            String mask = "000000000000";
            DecimalFormat df = new DecimalFormat(mask);
            snumber = df.format(number);

            // XXXnnnnnnnnn
            int billions = Integer.parseInt(snumber.substring(0,3));
            // nnnXXXnnnnnn
            int millions  = Integer.parseInt(snumber.substring(3,6));
            // nnnnnnXXXnnn
            int hundredThousands = Integer.parseInt(snumber.substring(6,9));
            // nnnnnnnnnXXX
            int thousands = Integer.parseInt(snumber.substring(9,12));

            String tradBillions;
            switch (billions) {
                case 0:
                    tradBillions = "";
                    break;
                case 1 :
                    tradBillions = convertLessThanOneThousand(billions)
                            + " billion ";
                    break;
                default :
                    tradBillions = convertLessThanOneThousand(billions)
                            + " billion ";
            }
            String result =  tradBillions;

            String tradMillions;
            switch (millions) {
                case 0:
                    tradMillions = "";
                    break;
                case 1 :
                    tradMillions = convertLessThanOneThousand(millions)
                            + " million ";
                    break;
                default :
                    tradMillions = convertLessThanOneThousand(millions)
                            + " million ";
            }
            result =  result + tradMillions;

            String tradHundredThousands;
            switch (hundredThousands) {
                case 0:
                    tradHundredThousands = "";
                    break;
                case 1 :
                    tradHundredThousands = "one thousand ";
                    break;
                default :
                    tradHundredThousands = convertLessThanOneThousand(hundredThousands)
                            + " thousand ";
            }
            result =  result + tradHundredThousands;

            String tradThousand;
            tradThousand = convertLessThanOneThousand(thousands);
            result =  result + tradThousand;

            // remove extra spaces!
            return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
        }

    public static String moneyString(BigDecimal d) {
        if (d == null) return "$0.00";

        return money.format(d);
    }


}
