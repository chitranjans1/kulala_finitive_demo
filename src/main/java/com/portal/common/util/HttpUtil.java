package com.portal.common.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by chit on 3/10/2017.
 */
public class HttpUtil {

    public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

    public static List<String> commaSeparatedStingToList(String input)
    {
        List<String> returnValue = new ArrayList<>();
        if (StringUtils.isNotBlank(input)) {
            StringTokenizer tokenizer = new StringTokenizer(input, ",");
            while (tokenizer.hasMoreTokens()) {
                returnValue.add(tokenizer.nextToken());
            }
        }
        return returnValue;
    }

    public static Map<String, String> getFiltersMap(String filters) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> filtersMap = mapper.readValue(filters, Map.class);
        return filtersMap;
    }

    public static boolean meetsFilterCriteria(String filters,  Map<String, Object> values) throws IOException
    {
        if (StringUtils.isNotBlank(filters))
        {
            Map<String, String> filtersMap = getFiltersMap(filters);
            for (String key : filtersMap.keySet())
            {
                String fieldVal = String.valueOf(values.get(key));
                String filterValues = filtersMap.get(key);
                String[] filterValuesArr = filterValues.split(",");
                boolean returnValueFromFilters = false;
                for (String filterVal : filterValuesArr) {
                    if (StringUtils.containsIgnoreCase(fieldVal, filterVal)) returnValueFromFilters = true;
                }
                if (!returnValueFromFilters) return returnValueFromFilters;

            }
        }
        return true;
    }
}
