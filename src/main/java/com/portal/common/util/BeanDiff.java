package com.portal.common.util;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyDescriptor;
import java.beans.Transient;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Date;

public class BeanDiff {
    private static final Log LOG = LogFactory.getLog(BeanDiff.class);
    private final PropertyDescriptor[] descriptors;
    private final Object oldObj;
    private final Object newObj;

    public BeanDiff(Object oldObj, Object newObj) {
        descriptors = PropertyUtils.getPropertyDescriptors(oldObj);
        this.oldObj = oldObj;
        this.newObj = newObj;
    }

    public String getDiffString() {
        StringBuilder sb = new StringBuilder();
        for (PropertyDescriptor d : descriptors) {
            try {
                final boolean readMethodIsTransient = d.getReadMethod() == null
                        || d.getReadMethod().getAnnotation(Transient.class) != null;
                final boolean writeMethodIsTransient = d.getWriteMethod() == null
                        || d.getWriteMethod().getAnnotation(Transient.class) != null;
                final boolean isTransient = readMethodIsTransient
                        || writeMethodIsTransient;

                Method writeMethod = d.getWriteMethod();
                if (writeMethod != null && !isTransient) {
                    Object p1 = PropertyUtils.getProperty(oldObj, d.getName());
                    Object p2 = PropertyUtils.getProperty(newObj, d.getName());
                    if (ObjectUtils.notEqual(p1, p2)) {
                        if (p1 instanceof BigDecimal && p2 instanceof BigDecimal) {
                            BigDecimal b1 = (BigDecimal)p1;
                            BigDecimal b2 = (BigDecimal)p2;
                            if (b1.compareTo(b2) != 0) {
                                sb.append(d.getName()).append("(").append(b2.toString()).append(") ");
                            }
                        } else if (p1 instanceof Date && p2 instanceof Date) {
                            Date d1 = (Date)p1;
                            Date d2 = (Date)p2;
                            if (d1.compareTo(d2) != 0) {
                                sb.append(d.getName()).append("(")
                                        .append(d2.toString())
                                        .append(") ");
                            }
                        } else if (d.getName().contains("Url")) {
                            sb.append(d.getName()).append("(...) ");
                        } else if (!d.getName().endsWith("BoxDetails")
                                && !d.getName().endsWith("BoxPass")) {
                            sb.append(d.getName()).append("(")
                                    .append(p2 == null ? "" : p2.toString())
                                    .append(") ");
                        }
                    }
                }
            } catch (Exception e) {
                //LOG.error(e.getMessage(), e);
            }
        }
        return sb.toString();
    }
}
