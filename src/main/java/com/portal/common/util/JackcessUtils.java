package com.portal.common.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class JackcessUtils {
    private static final Log LOG = LogFactory.getLog(JackcessUtils.class);

    public static String extractString(Object o) {
        if (o == null)
            return null;
        return o.toString();
    }

    public static Long extractLong(Object o) {
        if (o instanceof Number) {
            return ((Number) o).longValue();
        }
        return null;
    }

    public static BigDecimal extractBigDecimal(Object o) {
        if (o instanceof Number) {
            return new BigDecimal(o.toString());
        }
        return null;
    }

    public static Double extractDouble(Object o) {
        if (o instanceof Number) {
            return ((Number) o).doubleValue();
        }
        return null;
    }

    public static Boolean extractBoolean(Object o) {
        if (o instanceof Boolean) {
            return (Boolean) o;
        } else if (o instanceof String) {
            String s = o.toString();
        }
        return null;
    }

    public static LocalDateTime extractLocalDateTime(Object o) {
        if (o instanceof Date) {
            return DateUtils.asLocalDateTime((Date) o);
        } else if (o != null) {
            String s = o.toString();
        }
        return null;
    }

    public static LocalDate extractLocalDate(Object o) {
        if (o instanceof Date) {
            return DateUtils.asLocalDate((Date) o);
        } else if (o != null) {
            String s = o.toString();
        }
        return null;
    }
}
