package com.portal.common.util;

public final class Constants {
    public static final String UNDERWRITER = "underwriter";
    public static final String DECISION_COMMITTEE = "decisionCommittee";
    public static final String UNDERWRITER_US = "underwriterUS";
    public static final String LENDER = "lender";
    public static final String LENDER_HEADING = "Lender";
    public static final String SYSTEM = "system";
}
