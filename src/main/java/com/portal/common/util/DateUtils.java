package com.portal.common.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {
    private static final Log LOG = LogFactory.getLog(DateUtils.class);

    public static int diff360(LocalDate a, LocalDate b) {
        boolean reverseOrder = false;
        if (b.isBefore(a)) {
            LocalDate tmp = a;
            a = b;
            b = tmp;
            reverseOrder = true;
        }

        // US/NASD Method
        if (a.getMonth().equals(Month.FEBRUARY)
                && b.getMonth().equals(Month.FEBRUARY)
                && a.lengthOfMonth() == a.getDayOfMonth()
                && b.lengthOfMonth() == b.getDayOfMonth()) {
            // Case 1. Both a and b fall on last day of feb
            // => set b's day to 30s
            b = b.withDayOfMonth(30);
        }

        if (a.getDayOfMonth() == 31 ||
                (a.getMonth().equals(Month.FEBRUARY)
                        && a.lengthOfMonth() == a.getDayOfMonth())) {
            a = a.withDayOfMonth(30);
            if (b.getDayOfMonth() == 31) {
                b = b.withDayOfMonth(30);
            }
        }

        LOG.debug("A = " + a + "; B = " + b);
        int yearDiff = b.getYear() - a.getYear();
        int monthDiff = b.getMonthValue() - a.getMonthValue();
        int dayDiff = b.getDayOfMonth() - a.getDayOfMonth();

        return reverseOrder
                ? -(yearDiff*360 + monthDiff*30 + dayDiff)
                : (yearDiff*360 + monthDiff*30 + dayDiff);
    }

    public static String getDayOfMonthSuffix(final int n) {
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static long getEpochMillis(LocalDateTime ldt) {
        ZonedDateTime zdt = ldt.atZone(ZoneId.systemDefault());
        return zdt.toInstant().toEpochMilli();
    }

    public static Date asDate(LocalDateTime ldt) {
        if (ldt == null)
            return null;
        return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date asDate(LocalDate ld) {
        if (ld == null)
            return null;
        return Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime asLocalDateTime(Instant instant) {
        return LocalDateTime
                .ofInstant(
                        instant,
                        ZoneId.systemDefault());
    }

    public static LocalDateTime asLocalDateTime(long epochMillis) {
        return LocalDateTime
                .ofInstant(Instant.ofEpochMilli(epochMillis),
                        ZoneId.systemDefault());
    }

    /**
     * Calls {@link #asLocalDate(Date, ZoneId)} with the system default time zone.
     */
    public static LocalDate asLocalDate(Date date) {
        return asLocalDate(date, ZoneId.systemDefault());
    }

    /**
     * Creates {@link LocalDate} from {@code java.util.Date} or it's subclasses. Null-safe.
     */
    public static LocalDate asLocalDate(Date date, ZoneId zone) {
        if (date == null)
            return null;

        if (date instanceof java.sql.Date)
            return ((java.sql.Date) date).toLocalDate();
        else
            return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalDate();
    }

    /**
     * Calls {@link #asLocalDateTime(Date, ZoneId)} with the system default time zone.
     */
    public static LocalDateTime asLocalDateTime(Date date) {
        return asLocalDateTime(date, ZoneId.systemDefault());
    }

    /**
     * Creates {@link LocalDateTime} from {@code java.util.Date} or it's subclasses. Null-safe.
     */
    public static LocalDateTime asLocalDateTime(Date date, ZoneId zone) {
        if (date == null)
            return null;

        if (date instanceof java.sql.Timestamp)
            return ((java.sql.Timestamp) date).toLocalDateTime();
        else
            return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalDateTime();
    }

    /**
     * Calls {@link #asUtilDate(Object, ZoneId)} with the system default time zone.
     */
    public static Date asUtilDate(Object date) {
        return asUtilDate(date, ZoneId.systemDefault());
    }

    /**
     * Creates a {@link Date} from various date objects. Is null-safe. Currently supports:<ul>
     * <li>{@link Date}
     * <li>{@link java.sql.Date}
     * <li>{@link java.sql.Timestamp}
     * <li>{@link LocalDate}
     * <li>{@link LocalDateTime}
     * <li>{@link ZonedDateTime}
     * <li>{@link Instant}
     * </ul>
     *
     * @param zone Time zone, used only if the input object is LocalDate or LocalDateTime.
     * @return {@link Date} (exactly this class, not a subclass, such as java.sql.Date)
     */
    public static Date asUtilDate(Object date, ZoneId zone) {
        if (date == null)
            return null;

        if (date instanceof java.sql.Date || date instanceof java.sql.Timestamp)
            return new Date(((Date) date).getTime());
        if (date instanceof Date)
            return (Date) date;
        if (date instanceof LocalDate)
            return Date.from(((LocalDate) date).atStartOfDay(zone).toInstant());
        if (date instanceof LocalDateTime)
            return Date.from(((LocalDateTime) date).atZone(zone).toInstant());
        if (date instanceof ZonedDateTime)
            return Date.from(((ZonedDateTime) date).toInstant());
        if (date instanceof Instant)
            return Date.from((Instant) date);

        throw new UnsupportedOperationException("Don't know hot to convert " + date.getClass().getName() + " to java.util.Date");
    }

    /**
     * Creates an {@link Instant} from {@code java.util.Date} or it's subclasses. Null-safe.
     */
    public static Instant asInstant(Date date) {
        if (date == null)
            return null;
        else
            return Instant.ofEpochMilli(date.getTime());
    }

    /**
     * Calls {@link #asZonedDateTime(Date, ZoneId)} with the system default time zone.
     */
    public static ZonedDateTime asZonedDateTime(Date date) {
        return asZonedDateTime(date, ZoneId.systemDefault());
    }

    /**
     * Creates {@link ZonedDateTime} from {@code java.util.Date} or it's subclasses. Null-safe.
     */
    public static ZonedDateTime asZonedDateTime(Date date, ZoneId zone) {
        if (date == null)
            return null;
        else
            return asInstant(date).atZone(zone);
    }

    /**
     * Returns the maximum of two dates. A null date is treated as being less
     * than any non-null date.
     */
    public static Date max(Date d1, Date d2) {
        if (d1 == null && d2 == null) return null;
        if (d1 == null) return d2;
        if (d2 == null) return d1;
        return (d1.after(d2)) ? d1 : d2;
    }

    public static LocalDate max(LocalDate d1, LocalDate d2) {
        if (d1 == null && d2 == null) return null;
        if (d1 == null) return d2;
        if (d2 == null) return d1;
        return (d1.isAfter(d2)) ? d1 : d2;
    }

    /**
     * Returns the minimum of two dates. A null date is treated as being less
     * than any non-null date.
     */
    public static LocalDate min(LocalDate d1, LocalDate d2) {
        if (d1 == null || d2 == null) return null;
        return (d1.isBefore(d2)) ? d1 : d2;
    }

    //including d1, excluding d2:
    public static int daysBetween(LocalDate d1, LocalDate d2) {
        return (int) ChronoUnit.DAYS.between(d1, d2);
    }

    public static int monthsBetween(LocalDate d1, LocalDate d2) {
        return (int) ChronoUnit.MONTHS.between(d1, d2);
    }

    public static int monthsBetween(Date d1, Date d2) {
        Calendar startCalendar = new GregorianCalendar();
        if (d1 != null)
            startCalendar.setTime(d1);

        Calendar endCalendar = new GregorianCalendar();
        if (d2 != null)
            endCalendar.setTime(d2);

        int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
        return diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
    }

    public static String format(Date d) {
        if (d == null) return "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(d);
    }

    public static String format(LocalDate d) {
        return format(d, "yyyy-MM-dd");
    }

    public static String format(LocalDate d, String format) {
        if (d == null) return "";

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
        return d.format(dtf);
    }

    public static Date getStartOfMonth(Date date) {
        LocalDate monthStart = DateUtils.asLocalDate(date);
        return DateUtils.asDate(LocalDate.of(monthStart.getYear(), monthStart.getMonth(), 1));
    }

    public static LocalDate getStartOfMonth(LocalDate date) {
        LocalDate monthStart = date;
        return LocalDate.of(monthStart.getYear(), monthStart.getMonth(), 1);
    }

    public static Date getStartOfYear(Date date) {
        LocalDate monthStart = DateUtils.asLocalDate(date);
        return DateUtils.asDate(LocalDate.of(monthStart.getYear(), 1, 1));
    }

    public static LocalDate getStartOfYear(LocalDate date) {
        LocalDate monthStart = date;
        return LocalDate.of(monthStart.getYear(), 1, 1);
    }

    public static int daysInMonth(LocalDate d) {
        return d.lengthOfMonth();
    }

    public static int daysInMonth(Date d) {
        Calendar startCalendar = new GregorianCalendar();
        startCalendar.setTime(d);

        return startCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    // Returns true if dt >= sdate and dt < eDate
    public static boolean isBetween(LocalDate dt, LocalDate sDate, LocalDate eDate) {
        if (dt == null || sDate == null || eDate == null)
            return false;
        if ((dt.equals(sDate) || dt.isAfter(sDate)) && dt.isBefore(eDate))
            return true;

        return false;
    }

    // Returns true if dt >= sdate and dt < eDate
    public static boolean isBetween(Date dt, Date sDate, Date eDate) {
        if (dt == null || sDate == null || eDate == null)
            return false;
        if (((dt.getTime() == sDate.getTime()) || dt.after(sDate)) && dt.before(eDate))
            return true;

        return false;
    }

    public static String getQuarter(LocalDate dt) {
        if (dt == null)
            return null;
        int month = dt.getMonth().getValue();
        int year = dt.getYear();
        int quarter = (month / 3) + 1;
        return "Q" + quarter + " " + year;
    }

    public static Date covnertLocalDateToDate(LocalDate localDate) {
        return localDate == null ? null : Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime truncateToMillis(LocalDateTime localDateTime)
    {
        LocalDateTime existingDateTime = localDateTime == null ? LocalDateTime.now() : localDateTime.truncatedTo(ChronoUnit.MILLIS);
        return existingDateTime;
    }

    public static LocalDate toLocalDateVariableType(Object date)
    {
        if (date == null)
        {
            return null;
        }
        else if (date instanceof java.sql.Date)
        {
            return ((java.sql.Date)date).toLocalDate();
        }
        else if (date instanceof LocalDate)
        {
            return (LocalDate)date;
        }
        return null;
    }
}