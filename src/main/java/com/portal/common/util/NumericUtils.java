package com.portal.common.util;

import java.math.BigDecimal;

public class NumericUtils {

    public static final BigDecimal HUNDRED = BigDecimal.valueOf(100);

    public static BigDecimal getPercentAsDecimal(BigDecimal data) {
        return data != null ? data.divide(HUNDRED) : null;
    }

    public static BigDecimal getDecimalAsPercent(BigDecimal data) {
        return data != null ? data.multiply(HUNDRED) : null;
    }

    public static boolean isZeroOrNull(Number number)
    {
        return number == null || number.intValue() == 0;
    }

    public static boolean isNotZeroOrNull(Number number)
    {
        return !isZeroOrNull(number);
    }


    public static BigDecimal zeroIfNull(BigDecimal value) {
        if (value == null)
            return BigDecimal.ZERO;

        return value;
    }

    public static BigDecimal round(BigDecimal num)
    {
        return round(num,2);
    }

    public static BigDecimal round(BigDecimal num, int numDecPlaces)
    {
        return round(num,numDecPlaces,BigDecimal.ROUND_HALF_UP);
    }

    public static BigDecimal round(BigDecimal num, int numDecPlaces, int roundingMode)
    {
        if( num == null )
            return null;

        return num.setScale(numDecPlaces,roundingMode);
    }

    public static Long getNumberAsLong(Number number) {
        return Long.valueOf(number.longValue());
    }
}
