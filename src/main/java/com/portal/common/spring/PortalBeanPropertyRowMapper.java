package com.portal.common.spring;

import org.springframework.beans.BeanWrapper;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by chit on 5/2/2017.
 */
public class PortalBeanPropertyRowMapper<T> extends BeanPropertyRowMapper<T> {

    public PortalBeanPropertyRowMapper() {
        super();
    }

    /**
     * Create a new BeanPropertyRowMapper, accepting unpopulated properties
     * in the target bean.
     * <p>Consider using the {@link #newInstance} factory method instead,
     * which allows for specifying the mapped type once only.
     * @param mappedClass the class that each row should be mapped to
     */
    public PortalBeanPropertyRowMapper(Class<T> mappedClass) {
        super(mappedClass);
    }

    @Override
    protected void initBeanWrapper(BeanWrapper bw) {
        super.initBeanWrapper(bw);
        bw.setConversionService(new ConversionService() {
            @Override
            public boolean canConvert(Class<?> aClass, Class<?> aClass2) {
                return aClass == java.sql.Date.class && aClass2 == LocalDate.class || aClass == java.sql.Timestamp.class && aClass2 == LocalDateTime.class;
            }

            @Override
            public boolean canConvert(TypeDescriptor typeDescriptor, TypeDescriptor typeDescriptor2) {
                return canConvert(typeDescriptor.getType(), typeDescriptor2.getType());
            }

            @Override
            public <T> T convert(Object o, Class<T> tClass) {
                if (o instanceof Date && tClass == LocalDate.class) {
                    return (T) ((Date) o).toLocalDate();
                }

                if (o instanceof Timestamp && tClass == LocalDateTime.class) {
                    return (T) ((Timestamp) o).toLocalDateTime();
                }

                return null;


            }

            @Override
            public Object convert(Object o, TypeDescriptor typeDescriptor, TypeDescriptor typeDescriptor2) {
                return convert(o, typeDescriptor2.getType());
            }
        });
    }
}
