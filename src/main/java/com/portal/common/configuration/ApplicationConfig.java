package com.portal.common.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.hazelcast.config.*;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.portal.common.configuration.json.JsonLocalDataDeserializer;
import com.portal.common.configuration.json.JsonLocalDataTimeDeserializer;
import com.portal.common.configuration.json.JsonLocalDateSerializer;
import com.portal.common.configuration.json.JsonLocalDateTimeSerializer;
import com.portal.common.dal.PortalDB;
import com.portal.security.filter.NoCacheFilter;
import com.portal.security.filter.RememberMeFilter;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.spring.remoting.SecureRemoteInvocationExecutor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.AbstractFilter;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.Filter;
import javax.sql.DataSource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Application bean definitions.
 */
@Configuration
@PropertySource("classpath:application.properties")
@EnableWebMvc
@EnableCaching
@EnableTransactionManagement
@ComponentScan("com.portal")
public class ApplicationConfig extends WebMvcConfigurerAdapter implements TransactionManagementConfigurer {


    @Autowired
    private Environment env;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/ext/**")
                .addResourceLocations("/ext/");
    }

     /**
     * Used by the SecurityManager to access security data (users, roles, etc).
     * Many other realm implementations can be used too (PropertiesRealm,
     * LdapRealm, etc.
     *
     * @return
     */
    @Bean
    protected JdbcRealm jdbcRealm() {

        JdbcRealm jdbcRealm = new JdbcRealm();
        jdbcRealm.setName("jdbcRealm");
        jdbcRealm.setCredentialsMatcher(new PasswordMatcher());
        jdbcRealm.setDataSource(portalDataSource());

        jdbcRealm.setAuthenticationQuery("SELECT password FROM users WHERE user_name = ?");
        jdbcRealm.setPermissionsQuery("SELECT permission_name FROM ROLES_PERMISSIONS WHERE role_name = ?");
        jdbcRealm.setPermissionsLookupEnabled(false);
        jdbcRealm.setAuthorizationCachingEnabled(false);
        return jdbcRealm;
    }


    /**
     * Let's use some enterprise caching support for better performance.  You can replace this with any enterprise
     * caching framework implementation that you like (Terracotta+Ehcache, Coherence, GigaSpaces, etc
     *
     * @return
     */
    @Bean
    protected EhCacheManager cacheManagerShiro() {

        EhCacheManager ehCacheManager = new EhCacheManager();
        ehCacheManager.setCacheManager(((EhCacheCacheManager)ehCacheManager()).getCacheManager());
        return ehCacheManager;
    }

    /**
     * Secure Spring remoting:  Ensure any Spring Remoting method invocations can be associated
     * with a Subject for security checks.
     *
     * @param securityManager
     * @return
     */
    @Bean
    protected SecureRemoteInvocationExecutor secureRemoteInvocationExecutor(SecurityManager securityManager) {

        SecureRemoteInvocationExecutor executor = new SecureRemoteInvocationExecutor();
        executor.setSecurityManager(securityManager);

        return executor;
    }

    @Bean
    @PortalDB
    public DataSource portalDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(env.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(env.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(env.getRequiredProperty("jdbc.password"));
        return dataSource;
    }

    @Bean
    @PortalDB
    public JdbcTemplate portalJdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        return jdbcTemplate;
    }

    @Bean
    @PortalDB
    public NamedParameterJdbcTemplate portalNamedParameterJdbcTemplate(JdbcTemplate jdbcTemplate) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        return namedParameterJdbcTemplate;
    }

    @Bean
    public Map<String, String> filterChainDefinitionsMap() {
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<String, String>();
        filterChainDefinitionMap.put("/rest/pub/**", "anon");
        filterChainDefinitionMap.put("/ext/pub/**", "anon");

        filterChainDefinitionMap.put("/rest/priv/**", "anon");
        filterChainDefinitionMap.put("/ext/prv/**", "anon");

        filterChainDefinitionMap.put("/logout", "logout");
        filterChainDefinitionMap.put("/**", "authc");

        return filterChainDefinitionMap;
    }

   @Bean(name = "shiroFilter")
    public AbstractShiroFilter shiroFilter() {
        ShiroFilterFactoryBean factoryBean = new ShiroFilterFactoryBean();
        factoryBean.setSecurityManager(getDefaultWebSecurityManager());
        Map<String, Filter> filterMap = new LinkedHashMap<>();
        filterMap.put("authc", getRememberMeFilter());
        factoryBean.setFilters(filterMap);
        factoryBean.setLoginUrl("/ext/prv/index.html"); //TODO: implement login page
        factoryBean.setSuccessUrl("/ext/prv/index.html");
        factoryBean.setFilterChainDefinitionMap(filterChainDefinitionsMap());
        try {
            return (AbstractShiroFilter) factoryBean.getObject();
        } catch (Exception e) {
            throw new IllegalStateException("Cannot build shiroFilter", e);
        }
    }

    @Bean(name = "cacheFilter")
    public Filter getCacheFilter() {
        NoCacheFilter filter = new NoCacheFilter();
        return filter;
    }

    @Bean
    public AbstractFilter getRememberMeFilter() {
        RememberMeFilter filter = new RememberMeFilter();
        return filter;
    }

    @Bean
    public DefaultWebSecurityManager getDefaultWebSecurityManager() {
        DefaultWebSecurityManager dwsm = new DefaultWebSecurityManager();
        dwsm.setRealm(jdbcRealm());
        dwsm.setCacheManager(cacheManagerShiro());
        return dwsm;
    }

    @Bean
    public CacheManager ehCacheManager() {
        return new EhCacheCacheManager(ehCacheCacheManager().getObject());
    }

    @Bean
    public EhCacheManagerFactoryBean ehCacheCacheManager() {
        EhCacheManagerFactoryBean cmfb = new EhCacheManagerFactoryBean();
        cmfb.setConfigLocation(new ClassPathResource("ehcache.xml"));
        cmfb.setShared(true);
        return cmfb;
    }

    @Bean
    public ObjectMapper jacksonObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.setDateFormat(new SimpleDateFormat("MM/dd/yyyy"));
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addDeserializer(LocalDateTime.class,new JsonLocalDataTimeDeserializer());
        javaTimeModule.addDeserializer(LocalDate.class,new JsonLocalDataDeserializer());
        javaTimeModule.addSerializer(LocalDateTime.class,new JsonLocalDateTimeSerializer());
        javaTimeModule.addSerializer(LocalDate.class,new JsonLocalDateSerializer());
        objectMapper.registerModule(javaTimeModule);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        objectMapper.setFilterProvider(new SimpleFilterProvider().setFailOnUnknownId(false));

        return objectMapper;
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }

    /**
     * Transaction Manager
     * @return
     */
    @Bean
    @Override
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return new DataSourceTransactionManager(portalDataSource());
    }

    @Bean
    protected HazelcastInstance hazelcastInstance() {
        Config cfg = new ClasspathXmlConfig("hazelcast.xml");

        String nodes = env.getProperty("hazelcast.nodes");
        if(StringUtils.isNotEmpty(nodes))
        {
            TcpIpConfig tcpIpConfig = cfg.getNetworkConfig().getJoin().getTcpIpConfig();
            List<String> members = new ArrayList<>(Arrays.asList(StringUtils.split(nodes, "\\,")));
            tcpIpConfig.setMembers(members);
        }
        HazelcastInstance instance = Hazelcast.newHazelcastInstance(cfg);
        return instance;
    }
}