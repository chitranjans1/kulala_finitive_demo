package com.portal.common.configuration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.*;
import java.util.EnumSet;

/**
 * Initializes Spring Environment without the need for a web.xml
 */
public class ApplicationInitialization implements WebApplicationInitializer {

    private int MAX_UPLOAD_SIZE = 50 * 1024 * 1024;

    @Override
    public void onStartup(ServletContext container) {

        //now add the annotations
        AnnotationConfigWebApplicationContext appContext = getContext();

        // Manage the lifecycle of the root application context
        container.addListener(new ContextLoaderListener(appContext));

        FilterRegistration.Dynamic shiroFilter = container.addFilter("shiroFilter", DelegatingFilterProxy.class);
        shiroFilter.setInitParameter("targetFilterLifecycle", "true");
        shiroFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/*");


        FilterRegistration.Dynamic headersFilter = container.addFilter("cacheFilter", DelegatingFilterProxy.class);
        headersFilter.setInitParameter("targetFilterLifecycle", "true");
        headersFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");


        ServletRegistration.Dynamic remotingDispatcher = container.addServlet("remoting", new DispatcherServlet(appContext));
        remotingDispatcher.setLoadOnStartup(1);
        remotingDispatcher.addMapping("/remoting/*");


        ServletRegistration.Dynamic dispatcher = container.addServlet("DispatcherServlet", new DispatcherServlet(appContext));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");

        MultipartConfigElement multipartConfigElement = new MultipartConfigElement(System.getProperty("java.io.tmpdir"),
                MAX_UPLOAD_SIZE, MAX_UPLOAD_SIZE * 2, MAX_UPLOAD_SIZE / 2);

        dispatcher.setMultipartConfig(multipartConfigElement);

    }

    private AnnotationConfigWebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.setConfigLocation(getClass().getPackage().getName());
        return context;
    }

}