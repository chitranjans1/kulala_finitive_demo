package com.portal.common.exception;

/**
 * Created by chit on 6/30/2017.
 */
public class PortalRuntimeException extends RuntimeException {

    public PortalRuntimeException(Throwable t) {
        super (t);
    }

    public PortalRuntimeException(String s) {
        super (s);
    }

}
