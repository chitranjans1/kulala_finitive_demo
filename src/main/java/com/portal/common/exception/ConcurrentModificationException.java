package com.portal.common.exception;

/**
 * Created by chit on 7/12/2017.
 */
public class ConcurrentModificationException extends  PortalRuntimeException {

    public ConcurrentModificationException(Throwable t) {
        super (t);
    }

}
