package com.portal.common.exception;

/**
 * Created by chit on 7/12/2017.
 */
public class ValidationException extends  PortalRuntimeException {

    public ValidationException(String message) {
        super (message);
    }

}
