package com.portal.common.exception.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.common.exception.LogException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class GenericExceptionHandler {

    private static transient final Logger LOG = LoggerFactory.getLogger(GenericExceptionHandler.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private LogException logException;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> exceptionHandler(Exception ex) {
        Map<String,String> map = new HashMap<>();
        map.put("status","error");
        map.put("clientMsg","Unknown error has occurred. Please contact support.");
        map.put("systemMsg",ex.getMessage());

        LOG.error("Unknown error occurred",ex);
//        logException.log(ex);
        //TODO: better logging

        try {
            return new ResponseEntity<>(objectMapper.writeValueAsString(map), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
