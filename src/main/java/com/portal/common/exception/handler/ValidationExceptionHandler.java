package com.portal.common.exception.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.common.exception.LogException;
import com.portal.common.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ValidationExceptionHandler {

    private static transient final Logger LOG = LoggerFactory.getLogger(ValidationExceptionHandler.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private LogException logException;

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Object> exceptionHandler(ValidationException ex) {
        Map<String,String> map = new HashMap<>();
        map.put("status","validation");
        map.put("clientMsg",ex.getMessage());
        map.put("systemMsg",ex.getMessage());

//        LOG.error("Error occurred with "+request.getContextPath(),ex);
//        logException.log(ex);
        //TODO: better logging

        try {
            return new ResponseEntity<>(objectMapper.writeValueAsString(map), HttpStatus.BAD_REQUEST);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
