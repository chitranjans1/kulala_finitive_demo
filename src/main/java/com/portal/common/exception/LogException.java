package com.portal.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class LogException {

    private static transient final Logger LOG = LoggerFactory.getLogger(LogException.class);

    public void log (PortalRuntimeException re) {
        LOG.error("Error occurred ", re);
    }
}
