package com.portal.common.dal;

public interface JdbcParamArrayCreator<T> {
    Object[] createParam(T obj);
}
