package com.portal.common.dal;

import com.portal.common.helper.JDBCutil;
import com.portal.common.spring.PortalBeanPropertyRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractIdJsonRepository<T extends GenericIdJsonDB> {

    private static transient final Logger LOG = LoggerFactory.getLogger(AbstractIdJsonRepository.class);

    @Autowired
    @PortalDB
    private JdbcTemplate jdbcTemplate;

    protected abstract String getTableName();
    protected abstract Class<T> getBeanType();

    public List<T> getAll() {
        List<T> objects = jdbcTemplate.query("select * from "+getTableName(),
                new PortalBeanPropertyRowMapper<>(getBeanType()));
        return  objects;
    }

    public Long save(T instance) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(instance.getJson());
        paramsList.add(instance.getCreatedBy());
        paramsList.add(instance.getCreationDate());
        paramsList.add(instance.getLastUpdatedBy());
        paramsList.add(instance.getLastUpdateDate());

        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        StringBuilder sbSql = new StringBuilder("insert into "+getTableName()+" (");
        sbSql.append("json,created_by,creation_date,last_updated_by,last_update_date")
                .append(") ")
                .append(JDBCutil.generateValuesPlaceHolderClause(paramArr.length))
                .append(" returning id");

        Long id = jdbcTemplate.queryForObject(sbSql.toString(),paramArr,Long.class);
        instance.setId(id);
        return id;
    }

    public void update(T instance) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(instance.getJson());
        paramsList.add(instance.getLastUpdatedBy());
        paramsList.add(instance.getLastUpdateDate());
        paramsList.add(instance.getId());
        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        int count = jdbcTemplate.update(
                "update "+getTableName()+" set json = ?, last_updated_by = ?, last_update_date = ? where id = ? ",
                paramArr);
        LOG.info("Updated "+count+" "+getTableName());
    }

    public void delete(Long id) {
        jdbcTemplate.update("delete from "+getTableName()+" where id = ?", new Object[]{id});
        LOG.info("Deleted "+id+" from "+getTableName());
    }
}
