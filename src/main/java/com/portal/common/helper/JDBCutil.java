package com.portal.common.helper;

import com.portal.common.dal.JdbcParamArrayCreator;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public abstract class JDBCutil {

    public static void convertLocalDateParams(Object[] paramObjArray)
    {
        if( paramObjArray == null )
            return;

        for (int i = 0; i < paramObjArray.length; i++) {

            if( paramObjArray[i] != null && paramObjArray[i] instanceof LocalDate )
            {
                paramObjArray[i] = java.sql.Date.valueOf((LocalDate)paramObjArray[i]);
            }

            if( paramObjArray[i] != null && paramObjArray[i] instanceof LocalDateTime)
            {
                paramObjArray[i] = getTimestamp((LocalDateTime)paramObjArray[i]);
            }
        }
    }

    public static Timestamp getTimestamp(LocalDateTime localDateTime) {
        return Timestamp.valueOf(localDateTime);
    }

    public static Integer getInteger(ResultSet rs, String column) throws SQLException {
        int value = rs.getInt(column);
        return rs.wasNull() ? null : value;
    }

    public static Long getLong(ResultSet rs, String column) throws SQLException {
        long value = rs.getLong(column);
        return rs.wasNull() ? null : value;
    }

    public static Boolean getBoolean(ResultSet rs, String column) throws SQLException {
        boolean value = rs.getBoolean(column);
        return rs.wasNull() ? null : value;
    }

    public static LocalDate getLocalDate(ResultSet rs, String column) throws SQLException {
        Date value = rs.getDate(column);
        if( value != null )
            return value.toLocalDate();
        else
            return null;
    }

    public static LocalDateTime getLocalDateTime(ResultSet rs, String column) throws SQLException {
        Timestamp value = rs.getTimestamp(column);
        if( value != null )
            return value.toLocalDateTime();
        else
            return null;
    }

    public static String generateValuesPlaceHolderClause(int numOfParams)
    {
        StringBuilder sb = new StringBuilder("values(");
        for (int i = 0; i < numOfParams-1; i++) {
            sb.append("?,");
        }
        sb.append("?)");
        return sb.toString();
    }

    public static <T> void insertBatch(JdbcTemplate jdbcTemplate, int batchSize, String sql, List<T> dataList, JdbcParamArrayCreator<T> jdbcParamArrayCreator) {

        List<Object[]> paramList = new ArrayList<>();

        for (T item : dataList) {

            paramList.add(jdbcParamArrayCreator.createParam(item));

            if( paramList.size() >= batchSize)
            {
                jdbcTemplate.batchUpdate(sql,paramList);
                paramList.clear();
            }
        }

        if( !paramList.isEmpty() )
        {
            jdbcTemplate.batchUpdate(sql,paramList);
            paramList.clear();
        }
    }
}
