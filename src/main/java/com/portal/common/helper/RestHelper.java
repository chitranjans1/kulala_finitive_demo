package com.portal.common.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
public class RestHelper {

    @Autowired
    private RestTemplate restTemplate;

    public <T> ResponseEntity<T> get(String url, HttpHeaders headers, Class<T> entityType)
    {
        return restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), entityType);
    }

    public <T> ResponseEntity<T> post(String url, String body, HttpHeaders headers, Class<T> entityType)
    {
        return restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(body,headers), entityType);
    }

    public <T> ResponseEntity<T> put(String url, String body, HttpHeaders headers, Class<T> entityType)
    {
        return restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(body,headers), entityType);
    }

    public <T> ResponseEntity<T> delete(String url, String body, HttpHeaders headers, Class<T> entityType)
    {
        return restTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(body,headers), entityType);
    }

    public <T> ResponseEntity<T> postMultiPartFormRequest(String url, MultiValueMap<String, Object> multiValueMap,Class<T> entityType)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(multiValueMap, headers);

        return restTemplate.exchange(url,HttpMethod.POST,requestEntity,entityType);
    }
}
