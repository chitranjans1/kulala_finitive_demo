package com.portal.common.helper;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;

@Component
public class JsonHelper {

    public static final String DISPLAY_FIELD_DEF_FILTER = "displayFieldDefFilter";
    public static final String SCREEN_ID_FILTER = "screenIdFilter";

    public FilterProvider getFilterProvider(Pair<String,PropertyFilter>... filters) {
        SimpleFilterProvider simpleFilterProvider = new SimpleFilterProvider();
        for (Pair<String, PropertyFilter> filter : filters) {
            simpleFilterProvider.addFilter(filter.getKey(),filter.getValue());
        }
        return simpleFilterProvider;
    }

    public Pair<String,PropertyFilter> getDisplayFieldDefinitionFilterPair() {
        return Pair.of(DISPLAY_FIELD_DEF_FILTER,SimpleBeanPropertyFilter.serializeAllExcept("meta","value"));
    }

    public Pair<String,PropertyFilter> getScreenIdFilterPair() {
        return Pair.of(SCREEN_ID_FILTER,SimpleBeanPropertyFilter.serializeAllExcept("subScreens","displayGroups","type"));
    }
}
