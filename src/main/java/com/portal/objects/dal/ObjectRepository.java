package com.portal.objects.dal;

import com.portal.common.dal.PortalDB;
import com.portal.objects.dal.bean.PortalObjectDB;
import com.portal.common.helper.JDBCutil;
import com.portal.common.spring.PortalBeanPropertyRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ObjectRepository {

    private static transient final Logger LOG = LoggerFactory.getLogger(ObjectRepository.class);

    @Autowired
    @PortalDB
    private JdbcTemplate jdbcTemplate;

    public List<PortalObjectDB> getObjects() {
        List<PortalObjectDB> objects = jdbcTemplate.query("select * from objects",
                new PortalBeanPropertyRowMapper<>(PortalObjectDB.class));
        return  objects;
    }

    public Long save(PortalObjectDB object) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(object.getName());
        paramsList.add(object.getJson());
        paramsList.add(object.getCreatedBy());
        paramsList.add(object.getCreationDate());
        paramsList.add(object.getLastUpdatedBy());
        paramsList.add(object.getLastUpdateDate());

        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        StringBuilder sbSql = new StringBuilder("insert into objects (");
        sbSql.append("name,json,created_by,creation_date,last_updated_by,last_update_date")
                .append(") ")
                .append(JDBCutil.generateValuesPlaceHolderClause(paramArr.length))
                .append(" returning id");

        Long id = jdbcTemplate.queryForObject(sbSql.toString(),paramArr,Long.class);
        object.setId(id);
        return id;
    }

    public void update(PortalObjectDB object) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(object.getName());
        paramsList.add(object.getJson());
        paramsList.add(object.getLastUpdatedBy());
        paramsList.add(object.getLastUpdateDate());
        paramsList.add(object.getId());
        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        int count = jdbcTemplate.update(
                "update objects set name = ?, json = ?, last_updated_by = ?, last_update_date = ? where id = ? ",
                paramArr);
        LOG.info("Updated "+count+" objects");
    }

    public void delete(Long objectId) {
        jdbcTemplate.update("delete from objects where id = ?", new Object[]{objectId});
        LOG.info("Deleted "+objectId+" from objects");
    }
}
