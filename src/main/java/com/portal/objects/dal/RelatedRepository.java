package com.portal.objects.dal;

import com.portal.common.dal.PortalDB;
import com.portal.objects.dal.bean.RelatedDB;
import com.portal.common.helper.JDBCutil;
import com.portal.common.spring.PortalBeanPropertyRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RelatedRepository {

    private static transient final Logger LOG = LoggerFactory.getLogger(RelatedRepository.class);

    @Autowired
    @PortalDB
    private JdbcTemplate jdbcTemplate;

    public List<RelatedDB> getRelateds() {
        List<RelatedDB> relateds = jdbcTemplate.query("select * from relateds",
                new PortalBeanPropertyRowMapper<>(RelatedDB.class));
        return  relateds;
    }

    public Long save(RelatedDB relatedDB) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(relatedDB.getObjectId());
        paramsList.add(relatedDB.getJson());
        paramsList.add(relatedDB.getCreatedBy());
        paramsList.add(relatedDB.getCreationDate());
        paramsList.add(relatedDB.getLastUpdatedBy());
        paramsList.add(relatedDB.getLastUpdateDate());

        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        StringBuilder sbSql = new StringBuilder("insert into relateds (");
        sbSql.append("object_id,json,created_by,creation_date,last_updated_by,last_update_date")
                .append(") ")
                .append(JDBCutil.generateValuesPlaceHolderClause(paramArr.length))
                .append(" returning id");

        Long id = jdbcTemplate.queryForObject(sbSql.toString(),paramArr,Long.class);
        relatedDB.setId(id);
        return id;
    }

    public void update(RelatedDB relatedDB) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(relatedDB.getJson());
        paramsList.add(relatedDB.getLastUpdatedBy());
        paramsList.add(relatedDB.getLastUpdateDate());
        paramsList.add(relatedDB.getId());
        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        int count = jdbcTemplate.update(
                "update relateds set json = ?, last_updated_by = ?, last_update_date = ? where id = ? ",
                paramArr);
        LOG.info("Updated "+count+" related");
    }

    public void delete(Long relatedId) {
        jdbcTemplate.update("delete from relateds where id = ?", new Object[]{relatedId});
        LOG.info("Deleted "+relatedId+" from related");
    }
}
