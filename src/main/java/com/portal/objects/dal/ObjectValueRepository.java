package com.portal.objects.dal;

import com.portal.common.dal.AbstractIdJsonRepository;
import com.portal.objects.dal.bean.ObjectRelatedValueDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class ObjectValueRepository extends AbstractIdJsonRepository<ObjectRelatedValueDB> {

    private static transient final Logger LOG = LoggerFactory.getLogger(ObjectValueRepository.class);


    @Override
    protected String getTableName() {
        return "object_related_values";
    }

    @Override
    protected Class<ObjectRelatedValueDB> getBeanType() {
        return ObjectRelatedValueDB.class;
    }
}
