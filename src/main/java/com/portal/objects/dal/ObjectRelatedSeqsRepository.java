package com.portal.objects.dal;

import com.portal.common.dal.PortalDB;
import com.portal.common.helper.JDBCutil;
import com.portal.common.spring.PortalBeanPropertyRowMapper;
import com.portal.objects.dal.bean.ObjectRelatedSeqDB;
import com.portal.objects.dal.bean.PortalObjectDB;
import org.apache.commons.lang.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ObjectRelatedSeqsRepository {

    private static transient final Logger LOG = LoggerFactory.getLogger(ObjectRelatedSeqsRepository.class);

    @Autowired
    @PortalDB
    private JdbcTemplate jdbcTemplate;

    public List<ObjectRelatedSeqDB> getAll() {
        List<ObjectRelatedSeqDB> sequences = jdbcTemplate.query("select * from object_related_seqs",
                new PortalBeanPropertyRowMapper<>(ObjectRelatedSeqDB.class));
        return  sequences;
    }

    public void initSeq(Long objectId,Long relatedId) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(objectId);
        paramsList.add(relatedId);
        paramsList.add(Long.valueOf(0l));

        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        StringBuilder sbSql = new StringBuilder("insert into object_related_seqs (");
        sbSql.append("object_id,related_id,pk_seq_value")
                .append(") ")
                .append(JDBCutil.generateValuesPlaceHolderClause(paramArr.length));

        jdbcTemplate.update(sbSql.toString(),paramArr);
    }

    public void update(Long objectId,Long relatedId,Long pkSeqValue) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(pkSeqValue);
        paramsList.add(objectId);
        paramsList.add(ObjectUtils.defaultIfNull(relatedId,-1l));
        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        int count = jdbcTemplate.update(
                "update object_related_seqs set pk_seq_value = ? where object_id = ? and coalesce(related_id,-1) = ?",
                paramArr);
        LOG.info("Updated "+count+" object_related_seqs");
    }
}
