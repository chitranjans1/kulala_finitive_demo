package com.portal.objects.dal.bean;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
public class PortalObjectDB implements Serializable {

    private String name;
    private Long id;
    private String json;
    private String createdBy;
    private LocalDateTime creationDate;
    private String lastUpdatedBy;
    private LocalDateTime lastUpdateDate;
}
