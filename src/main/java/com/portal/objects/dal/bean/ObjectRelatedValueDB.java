package com.portal.objects.dal.bean;

import com.portal.common.dal.GenericIdJsonDB;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObjectRelatedValueDB extends GenericIdJsonDB {

}
