package com.portal.objects.dal.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObjectRelatedSeqDB {
    private Long objectId;
    private Long relatedId;
    private Long pkSeqValue;
}
