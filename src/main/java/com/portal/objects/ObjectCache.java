package com.portal.objects;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import com.hazelcast.core.IMap;
import com.portal.displaygroup.domain.DisplayFieldValue;
import com.portal.objects.dal.ObjectRelatedSeqsRepository;
import com.portal.objects.dal.bean.ObjectRelatedSeqDB;
import com.portal.objects.domain.Field;
import com.portal.objects.domain.ObjectRelatedValue;
import com.portal.objects.dal.bean.ObjectRelatedValueDB;
import com.portal.objects.dal.bean.PortalObjectDB;
import com.portal.objects.dal.bean.RelatedDB;
import com.portal.objects.dal.ObjectRepository;
import com.portal.objects.dal.ObjectValueRepository;
import com.portal.objects.dal.RelatedRepository;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ObjectCache {

    @Autowired
    private ObjectRepository objectRepository;

    @Autowired
    private ObjectValueRepository objectValueRepository;

    @Autowired
    private RelatedRepository relatedRepository;

    @Autowired
    private ObjectRelatedSeqsRepository objectRelatedSeqsRepository;

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Autowired
    private ObjectMapper objectMapper;

    public static final String OBJECT_DEFINITION_BY_ID_CACHE = "objectDefintionByIdCache";
    public static final String OBJECT_DEFINITION_BY_NAME_CACHE = "objectDefintionByNameCache";
    public static final String OBJECT_RELATED_VALUE_CACHE = "objectRelatedValueCache";
    public static final String RELATED_DEFINITION_CACHE = "relatedDefinitionCache";

    private String getObjectRelatedCacheKey(Long objectId, Long relatedId) {
        return objectId+"|"+relatedId;
    }

    private String getObjectRelatedValueCacheKey(Long objectId, Long relatedId, Long pk) {
        return getObjectRelatedCacheKey(objectId,relatedId)+"|"+pk;
    }

    public void saveObjectValue(ObjectRelatedValueDB valueDB, ObjectRelatedValue value) {
        objectValueRepository.save(valueDB);
        addToCache(valueDB,value);
    }

    public void updateObjectValue(ObjectRelatedValueDB valueDB, ObjectRelatedValue value) {
        objectValueRepository.update(valueDB);
        addToCache(valueDB,value);
    }

    public void saveObjectDefinition(PortalObjectDB object) {
        objectRepository.save(object);
        getObjectDefByIdCache().put(object.getId(), object);
        getObjectDefByNameCache().put(object.getName(), object);

        objectRelatedSeqsRepository.initSeq(object.getId(),null);
        getSeqCache(object.getId(),null).getAndSet(0l);
    }

    public void updateObjectDefinition(PortalObjectDB object) {
        objectRepository.update(object);
        getObjectDefByIdCache().put(object.getId(), object);
        getObjectDefByNameCache().put(object.getName(), object);
    }

    public PortalObjectDB getObject(Long objectId) {
       return getObjectDefByIdCache().get(objectId);
    }

    public PortalObjectDB getObject(String objectName) {
        return getObjectDefByNameCache().get(objectName);
    }

    public Pair<ObjectRelatedValueDB,ObjectRelatedValue> getObjectRelatedValuePair(Long objectId, Long relatedId, Long pk) {
        return getObjectRelatedValueCache().get(getObjectRelatedValueCacheKey(objectId,relatedId,pk));
    }

    public ObjectRelatedValueDB getObjectRelatedValueDB(Long objectId, Long relatedId, Long pk) {
        return getObjectRelatedValuePair(objectId,relatedId,pk).getKey();
    }

    public ObjectRelatedValue getObjectRelatedValue(Long objectId, Long relatedId, Long pk) {
        return getObjectRelatedValuePair(objectId,relatedId,pk).getValue();
    }

    public List<PortalObjectDB> getAllObjects() {
        IMap<Long, PortalObjectDB> cache = getObjectDefByIdCache();
        Set<Long> keySet = cache.keySet();

        List<PortalObjectDB> list = new ArrayList<>();
        for (Long key : keySet) {
            list.add(cache.get(key));
        }

        return list;
    }

    public void removeObject(Long objectId) {

        IMap<Long, PortalObjectDB> idCache = getObjectDefByIdCache();
        PortalObjectDB portalObjectDB = idCache.get(objectId);
        String name = portalObjectDB.getName();

        objectRepository.delete(objectId);
        idCache.remove(objectId);

        IMap<String, PortalObjectDB> nameCache = getObjectDefByNameCache();
        nameCache.remove(name);
    }

    public void updateRelatedDefinition(RelatedDB relatedDB) {

        if( relatedDB.getId() == null ) {
            relatedRepository.save(relatedDB);

            objectRelatedSeqsRepository.initSeq(relatedDB.getObjectId(),relatedDB.getId());
            getSeqCache(relatedDB.getObjectId(),relatedDB.getId()).getAndSet(0l);
        } else {
            relatedRepository.update(relatedDB);
        }

        getRelatedDefCache().put(relatedDB.getId(), relatedDB);
    }

    public RelatedDB getRelatedDB(Long relatedId) {
        return getRelatedDefCache().get(relatedId);
    }

    public void removeRelated(Long relatedId) {
        relatedRepository.delete(relatedId);
        IMap<Long,RelatedDB> cache = getRelatedDefCache();
        cache.remove(relatedId);
    }

    public List<RelatedDB> getAllRelated() {
        IMap<Long, RelatedDB> cache = getRelatedDefCache();
        Set<Long> keySet = cache.keySet();

        List<RelatedDB> list = new ArrayList<>();
        for (Long key : keySet) {
            list.add(cache.get(key));
        }

        return list;
    }

    public long getNextSeqVal(Long objectId,Long relatedId) {
        long seqValue = getSeqCache(objectId, relatedId).incrementAndGet();
        objectRelatedSeqsRepository.update(objectId,relatedId,seqValue);
        return seqValue;
    }

    @PostConstruct
    public void loanAllCaches() {
        loadObjectCache();
        loadRelatedCache();
        loadObjectRelatedValueCache();
        loadObjectRelatedSeqCache();
    }

    private IMap<Long,PortalObjectDB> getObjectDefByIdCache() {
        return hazelcastInstance.getMap(OBJECT_DEFINITION_BY_ID_CACHE);
    }

    private IMap<String,PortalObjectDB> getObjectDefByNameCache() {
        return hazelcastInstance.getMap(OBJECT_DEFINITION_BY_NAME_CACHE);
    }

    private IMap<String,Pair<ObjectRelatedValueDB,ObjectRelatedValue>> getObjectRelatedValueCache() {
        return hazelcastInstance.getMap(OBJECT_RELATED_VALUE_CACHE);
    }

    private IAtomicLong getSeqCache(Long objectId,Long relatedId) {
        return hazelcastInstance.getAtomicLong(getObjectRelatedCacheKey(objectId,relatedId));
    }

    private IMap<Long,RelatedDB> getRelatedDefCache() {
        return hazelcastInstance.getMap(RELATED_DEFINITION_CACHE);
    }

    private void addToCache(ObjectRelatedValueDB valueDB, ObjectRelatedValue value) {
        DisplayFieldValue displayFieldValue = value.getDisplayFieldValues().stream()
                .filter(v -> v.getMeta().getType().equals(Field.PRIMARY_KEY_FIELD))
                .findFirst()
                .get();

        Number numValue = (Number) displayFieldValue.getValue().getValue();
        Long pk = Long.valueOf(numValue.longValue());
        displayFieldValue.getValue().setValue(pk);

        getObjectRelatedValueCache().put(
                getObjectRelatedValueCacheKey(value.getObjectId(),value.getRelatedId(),pk),
                Pair.of(valueDB,value));
    }

    public void loadObjectCache() {
        List<PortalObjectDB> objects = objectRepository.getObjects();
        for (PortalObjectDB object : objects) {
            getObjectDefByIdCache().put(object.getId(), object);
            getObjectDefByNameCache().put(object.getName(), object);
        }
    }

    public void loadRelatedCache() {
        List<RelatedDB> relateds = relatedRepository.getRelateds();
        for (RelatedDB relatedDB : relateds) {
            getRelatedDefCache().put(relatedDB.getId(), relatedDB);
        }
    }

    public void loadObjectRelatedValueCache() {
        List<ObjectRelatedValueDB> dbs = objectValueRepository.getAll();
        for (ObjectRelatedValueDB db : dbs) {
            try {
                ObjectRelatedValue value = objectMapper.readValue(db.getJson(), ObjectRelatedValue.class);
                addToCache(db,value);
            } catch (IOException e) {
                throw new RuntimeException("Unable to parse value instance json for id "+db.getId());
            }
        }
    }

    public void loadObjectRelatedSeqCache() {
        List<ObjectRelatedSeqDB> dbs = objectRelatedSeqsRepository.getAll();
        for (ObjectRelatedSeqDB db : dbs) {
            getSeqCache(db.getObjectId(),db.getRelatedId()).set(db.getPkSeqValue());
        }
    }
}
