package com.portal.objects.domain;

import com.portal.displaygroup.domain.DisplayFieldValue;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ObjectRelatedValue implements Serializable {
    private Long id;
    private Long objectId;
    private Long relatedId;
    private List<DisplayFieldValue> displayFieldValues = new ArrayList<>();

    public DisplayFieldValue getValue(String fieldName) {
        return displayFieldValues.stream()
                .filter(v -> v.getFieldName().equalsIgnoreCase(fieldName))
                .findAny()
                .orElse(null);
    }
}
