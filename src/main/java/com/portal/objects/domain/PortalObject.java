package com.portal.objects.domain;

import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chit on 11/17/2017.
 */
public class PortalObject implements Serializable {

    private String name;
    private Long id;
    private List<Field> fields;
    private List<Related> relateds;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = ObjectUtils.defaultIfNull(fields, new ArrayList<Field>());
    }

    public List<Related> getRelateds() {
        return relateds;
    }

    public void setRelateds(List<Related> relateds) {
        this.relateds = ObjectUtils.defaultIfNull(relateds, new ArrayList<Related>());
    }

}
