package com.portal.objects.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Related implements Serializable {
    private Long id;
    private PortalObject object;
    private List<Field> relatedFields = new ArrayList<>();
}
