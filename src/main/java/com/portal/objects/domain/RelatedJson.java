package com.portal.objects.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
public class RelatedJson implements Serializable {

    private Long id;
    private String object1Name;
    private String object2Name;
    private List<Field> fields;


}
