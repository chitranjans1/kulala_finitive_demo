package com.portal.objects.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class RelatedIdOnlyJson implements Serializable {

    private Long id;
    private String object1Name;
    private String object2Name;

}
