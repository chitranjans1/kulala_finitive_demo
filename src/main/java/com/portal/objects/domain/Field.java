package com.portal.objects.domain;

import com.portal.common.domain.DropdownTitleValue;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class Field implements Serializable {

    public static final String PRIMARY_KEY_FIELD = "pk";
    public static final String EDITABLE_FIELD = "editable";

    private String name;
    private List<DropdownTitleValue> optionValues = new ArrayList<>();
    private Object defaultValue;
    private String type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Field field = (Field) o;
        return Objects.equals(name, field.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }
}
