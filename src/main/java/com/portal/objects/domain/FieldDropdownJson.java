package com.portal.objects.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FieldDropdownJson {
    private List<Field> objectFields;
    private List<RelatedJson> relatedFields;
}
