package com.portal.objects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.portal.common.domain.IdNameBean;
import com.portal.displaygroup.domain.DisplayField;
import com.portal.displaygroup.domain.DisplayFieldValue;
import com.portal.displaygroup.domain.DisplayGroup;
import com.portal.objects.dal.bean.ObjectRelatedValueDB;
import com.portal.objects.dal.bean.PortalObjectDB;
import com.portal.objects.dal.bean.RelatedDB;
import com.portal.objects.domain.*;
import com.portal.screens.domain.*;
import com.portal.common.exception.ValidationException;
import com.portal.screens.ScreenService;
import com.portal.security.access.user.UserService;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class
ObjectService {
    private static final Log LOG = LogFactory.getLog(ObjectService.class);

    @Autowired
    private ObjectCache objectCache;

    @Autowired
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ScreenService screenService;

    public Long saveObject(String objectJson) throws IOException {
        PortalObject portalObject = objectMapper.readValue(objectJson, PortalObject.class);

        List<Field> fields = portalObject.getFields();
        long numPkFields = fields.stream().filter(f -> f.getType().equalsIgnoreCase(Field.PRIMARY_KEY_FIELD)).count();
        if( numPkFields > 1 )
            throw new ValidationException("Object cannot have more than 1 primary key fields");

        if (portalObject.getId() != null) {
            return updateObject(portalObject, objectJson);
        } else {
            return insertObject(portalObject, objectJson);
        }
    }

    private Long updateObject(PortalObject portalObject, String objectJson) throws IOException {
        PortalObjectDB portalObjectDB = objectCache.getObject(portalObject.getId());

        PortalObject existingPortalObject = objectMapper.readValue(objectJson, PortalObject.class);
        validateFieldRemoval(portalObject.getFields(), existingPortalObject.getFields());

        portalObjectDB.setLastUpdateDate(LocalDateTime.now());
        portalObjectDB.setLastUpdatedBy(userService.getUserName());
        portalObjectDB.setJson(objectJson);
        portalObjectDB.setName(portalObject.getName());
        portalObjectDB.setId(portalObject.getId());

        objectCache.updateObjectDefinition(portalObjectDB);

        return portalObjectDB.getId();
    }

    private Long insertObject(PortalObject portalObject, String objectJson) throws IOException {

        if( objectCache.getObject(portalObject.getName()) != null )
            throw new ValidationException("Duplicate object name");

        PortalObjectDB portalObjectDB = new PortalObjectDB();
        portalObjectDB.setLastUpdateDate(LocalDateTime.now());
        portalObjectDB.setLastUpdatedBy(userService.getUserName());
        portalObjectDB.setCreationDate(LocalDateTime.now());
        portalObjectDB.setCreatedBy(userService.getUserName());
        portalObjectDB.setJson(objectJson);
        portalObjectDB.setName(portalObject.getName());

        objectCache.saveObjectDefinition(portalObjectDB);

        ObjectNode jsonNode = (ObjectNode)objectMapper.readTree(objectJson);
        jsonNode.put("id",portalObjectDB.getId());
        portalObjectDB.setJson(objectMapper.writeValueAsString(jsonNode));

        objectCache.updateObjectDefinition(portalObjectDB);

        return portalObjectDB.getId();
    }

    public String getObjectAndSubObjectsById(Long objectId) throws IOException {
        PortalObjectDB objectDB = objectCache.getObject(objectId);
        PortalObject portalObject = objectMapper.readValue(objectDB.getJson(), PortalObject.class);
        Map<Long, String> objects = new HashMap<>();
        objects.put(portalObject.getId(), portalObject.getName());
        for (Related related : portalObject.getRelateds()) {
            objects.put(related.getObject().getId(), related.getObject().getName());
        }
        return objectMapper.writeValueAsString(objects);
    }

    public String getRelatedObjectsById(Long objectId) throws IOException {
        PortalObjectDB objectDB = objectCache.getObject(objectId);
        PortalObject portalObject = objectMapper.readValue(objectDB.getJson(), PortalObject.class);
        Map<Long, String> objects = new HashMap<>();
        for (Related related : portalObject.getRelateds()) {
            objects.put(related.getObject().getId(), related.getObject().getName());
        }
        return objectMapper.writeValueAsString(objects);
    }

    public Map<String, Field> getFieldsByObjectId(Long objectId) throws IOException {
        PortalObjectDB objectDB = objectCache.getObject(objectId);
        PortalObject portalObject = objectMapper.readValue(objectDB.getJson(), PortalObject.class);
        return getFieldsByObject(portalObject);
    }

    public Map<String, Field> getFieldsByObjectIdRelatedId(Long objectId, Long relatedObjectId) throws IOException {
        PortalObjectDB objectDB = objectCache.getObject(objectId);
        PortalObject portalObject = objectMapper.readValue(objectDB.getJson(), PortalObject.class);
        for (Related related : portalObject.getRelateds()) {
            if (related.getObject().getId().equals(relatedObjectId))
                return getFieldsByObject(related.getObject());
        }
        return getFieldsByObject(portalObject);
    }

    private Map<String, Field> getFieldsByObject(PortalObject object) {
        Map<String, Field> fields = object.getFields().stream().collect(Collectors.toMap(field -> field.getName(), field -> field));
        return fields;
    }

    public String getObjectJsonById(Long objectId) {
        PortalObjectDB objectDB = objectCache.getObject(objectId);
        return objectDB.getJson();
    }

    public PortalObject getObjectById(Long objectId) throws IOException {
        String json = getObjectJsonById(objectId);
        PortalObject portalObject = objectMapper.readValue(json, PortalObject.class);
        portalObject.setRelateds(getRelatedByObjectId(portalObject.getId()));
        return portalObject;
    }

    public PortalObject getObjectByName(String objectName) throws IOException {
        PortalObjectDB objectDB = objectCache.getObject(objectName);
        PortalObject portalObject = objectMapper.readValue(objectDB.getJson(), PortalObject.class);
        portalObject.setRelateds(getRelatedByObjectId(portalObject.getId()));
        return portalObject;
    }

    public List<Related> getRelatedByObjectId(Long objectId) throws IOException {
        List<RelatedDB> relatedDBs = objectCache.getAllRelated().stream()
                .filter(r -> r.getObjectId().equals(objectId))
                .collect(Collectors.toList());

        List<Related> list = new ArrayList<>();
        for (RelatedDB relatedDB : relatedDBs) {

            RelatedJson relatedJson = objectMapper.readValue(relatedDB.getJson(), RelatedJson.class);
            PortalObject object2 = getObjectByName(relatedJson.getObject2Name());

            Related related = new Related();
            related.setId(relatedJson.getId());
            related.setRelatedFields(relatedJson.getFields());
            related.setObject(object2);

            list.add(related);
        }
        return list;

    }


    public Map<Long,String> getObjectIdsAsMap() {
        List<PortalObjectDB> objects = objectCache.getAllObjects();
        Map<Long,String> map = objects.stream().collect(
                Collectors.toMap(PortalObjectDB::getId, PortalObjectDB::getName));
        return map;
    }

    public List<IdNameBean> getObjectIdsAsList() {
        List<PortalObjectDB> objects = objectCache.getAllObjects();
        return objects.stream().map(o -> IdNameBean.of(o.getId(), o.getName())).collect(Collectors.toList());
    }

    public List<PortalObject> getAllObjects() throws IOException {
        List<PortalObject> objectsObj = new ArrayList<>();

        List<PortalObjectDB> objects = objectCache.getAllObjects();
        for (PortalObjectDB object : objects) {
            objectsObj.add(getObjectById(object.getId()));
        }

        return objectsObj;
    }

    public List<PortalObject> getAvailableObjects(Long objectId) throws IOException {

        PortalObject portalObject = getObjectById(objectId);

        List<PortalObject> objectsList = new ArrayList<>();
        List<PortalObjectDB> objects = objectCache.getAllObjects();
        for (PortalObjectDB object : objects) {
            if(!object.getId().equals(objectId) && !isObjectPresentInHierarchy(object, objectId)) {
                PortalObject tmpPortalObject = getObjectById(object.getId());

                boolean relatedExists = portalObject.getRelateds().stream()
                        .filter(r -> r.getObject().getName().equalsIgnoreCase(tmpPortalObject.getName()))
                        .findAny().isPresent();

                if( !relatedExists )
                    objectsList.add(tmpPortalObject);

            }
        }
        return objectsList;
    }

    private boolean isObjectPresentInHierarchy(PortalObjectDB object, Long objectId) throws IOException {
        if(objectId == object.getId() ) {
            return true;
        }
        PortalObject portalObject = getObjectById(object.getId());
        List<Related> relateds = portalObject.getRelateds();
        if(relateds != null) {
            for(int i=0;i<relateds.size();i++){
                Long childObjectId =  relateds.get(i).getObject().getId();
                if(objectId == childObjectId){
                    return true;
                }
                PortalObjectDB portalObjectDB = objectCache.getObject(childObjectId);
                if(isObjectPresentInHierarchy(portalObjectDB, objectId)){
                    return true;
                };
            }
        }
        return false;
    }

    public void deleteObject(Long objectId) throws IOException {
        PortalObject portalObject = getObjectById(objectId);

        List<Related> relateds = portalObject.getRelateds();
        if( !relateds.isEmpty() )
            throw new ValidationException("Cannot remove object, there are related objects associated");

        List<RelatedIdOnlyJson> allRelated = getAllRelated(null);
        for (RelatedIdOnlyJson relatedIdOnlyJson : allRelated) {
            if( portalObject.getName().equalsIgnoreCase(relatedIdOnlyJson.getObject1Name()) || portalObject.getName().equalsIgnoreCase(relatedIdOnlyJson.getObject2Name()) )
                throw new ValidationException("Cannot remove object, there are related objects associated");
        }

        validateObjectRemoval(portalObject);

        objectCache.removeObject(objectId);
    }

    private void validateObjectRemoval(PortalObject portalObject) throws IOException {
        List<IdNameBean> screenIds = screenService.getScreenIdsAsList();
        for (IdNameBean screenId : screenIds) {
            Screen screen = screenService.getScreenObjById(screenId.getId());

            recursiveCheckScreen(screen,displayField -> {
                if( portalObject.getName().equalsIgnoreCase(displayField.getFieldName()) )
                    throw new ValidationException("Cannot remove object, it is part of existing screen ("+screen.getName()+")");

                return true;
            });
        }
    }

    private void validateFieldRemoval(List<Field> currentFields, List<Field> existingFields) throws IOException {

        List<Field> removedFields = ListUtils.subtract(existingFields, currentFields);
        Set<String> removedFieldNames = removedFields.stream().map(f -> f.getName().toLowerCase()).collect(Collectors.toSet());

        List<IdNameBean> screenIds = screenService.getScreenIdsAsList();
        for (IdNameBean screenId : screenIds) {
            Screen screen = screenService.getScreenObjById(screenId.getId());

            recursiveCheckScreen(screen,displayField -> {
                if( removedFieldNames.contains(displayField.getFieldName()) )
                    throw new ValidationException("Cannot remove field, it is part of existing screen ("+screen.getName()+")");

                return true;
            });
        }
    }

    private void recursiveCheckScreen(Screen screen, Function<DisplayField,Boolean> function) {
        List<Screen> subScreens = screen.getSubScreens();
        for (Screen subScreen : subScreens) {
            recursiveCheckScreen(subScreen,function);
        }

        for (DisplayGroup displayGroup : screen.getDisplayGroups()) {
            recursiveCheckDisplayGroup(displayGroup,function);
        }
    }

    private void recursiveCheckDisplayGroup(DisplayGroup displayGroup, Function<DisplayField,Boolean> function) {
        List<DisplayGroup> subDisplayGroups = displayGroup.getSubDisplayGroups();
        for (DisplayGroup subDisplayGroup : subDisplayGroups) {
            recursiveCheckDisplayGroup(subDisplayGroup,function);
        }

        List<? extends DisplayField> displayFields = displayGroup.getDisplayFields();
        for (DisplayField displayField : displayFields) {
            function.apply(displayField);
        }
    }

    public RelatedDB saveRelated(String json) throws IOException {
        RelatedJson relatedJson = objectMapper.readValue(json, RelatedJson.class);
        PortalObject object1 = getObjectByName(relatedJson.getObject1Name());
        if( object1 == null )
            throw new ValidationException("Object "+relatedJson.getObject1Name()+" does not exist");

        PortalObject object2 = getObjectByName(relatedJson.getObject1Name());
        if( object2 == null )
            throw new ValidationException("Object "+relatedJson.getObject2Name()+" does not exist");

        RelatedDB relatedDB;
        if( relatedJson.getId() == null )
        {
            List<Related> relateds = object1.getRelateds();
            Related related = relateds.stream()
                    .filter(r -> r.getObject().getName().equalsIgnoreCase(relatedJson.getObject2Name()))
                    .findAny().orElse(null);
            if( related != null )
                throw new ValidationException("This object combination already exists for related id "+related.getId());

            relatedDB = new RelatedDB();
            relatedDB.setObjectId(object1.getId());
            relatedDB.setJson(json);

            relatedDB.setLastUpdateDate(LocalDateTime.now());
            relatedDB.setLastUpdatedBy(userService.getUserName());
            relatedDB.setCreationDate(LocalDateTime.now());
            relatedDB.setCreatedBy(userService.getUserName());
        }
        else
        {
            relatedDB = objectCache.getRelatedDB(relatedJson.getId());

            if( !relatedDB.getObjectId().equals(object1.getId()) )
                throw new ValidationException("Object Id cannot be changed on the related");

            RelatedJson existingRelatedJson = objectMapper.readValue(relatedDB.getJson(), RelatedJson.class);
            validateFieldRemoval(relatedJson.getFields(), existingRelatedJson.getFields());

            relatedDB.setJson(json);

            relatedDB.setLastUpdateDate(LocalDateTime.now());
            relatedDB.setLastUpdatedBy(userService.getUserName());
        }
        objectCache.updateRelatedDefinition(relatedDB);

        relatedJson.setId(relatedDB.getId());
        String newJson = objectMapper.writeValueAsString(relatedJson);
        relatedDB.setJson(newJson);

        objectCache.updateRelatedDefinition(relatedDB);

        return relatedDB;

    }

    public List<RelatedIdOnlyJson> getAllRelated(Long objectId) throws IOException {
        List<RelatedDB> allRelated = objectCache.getAllRelated();
        List<RelatedIdOnlyJson> list = new ArrayList<>();
        for (RelatedDB relatedDB : allRelated) {
            if( objectId == null || objectId.equals(relatedDB.getObjectId()))
                list.add(objectMapper.readValue(relatedDB.getJson(),RelatedIdOnlyJson.class));
        }
        return list;
    }

    public RelatedJson getRelatedJson(Long relatedId) throws IOException {
        RelatedDB relatedDB = objectCache.getRelatedDB(relatedId);
        return objectMapper.readValue(relatedDB.getJson(), RelatedJson.class);
    }

    public void deleteRelated(Long relatedId) throws IOException {
        RelatedJson related = getRelatedJson(relatedId);

        validateRelatedRemoval(related);

        objectCache.removeRelated(related.getId());
    }

    private void validateRelatedRemoval(RelatedJson relatedJson) throws IOException {
        //TODO: implement related on screen
//        List<IdNameBean> screenIds = screenService.getScreenIdsAsList();
//        for (IdNameBean screenId : screenIds) {
//            Screen screen = screenService.getScreenObjById(screenId.getId());
//
//            recursiveCheckScreen(screen,displayField -> {
//                if( portalObject.getName().equalsIgnoreCase(displayField.getFieldName()) )
//                    throw new ValidationException("Cannot remove object, it is part of existing screen ("+screen.getName()+")");
//
//                return true;
//            });
//        }
    }

    public List<Field> getObjectRelatedFields(Long objectId, Long relatedId) throws IOException {

        List<Field> fields = new ArrayList<>();

        if( relatedId != null ) {
            RelatedJson relatedJson = getRelatedJson(relatedId);
            fields.addAll(relatedJson.getFields());
        } else {
            PortalObject portalObject = getObjectById(objectId);
            fields.addAll(portalObject.getFields());
        }

        return fields;
    }

    public FieldDropdownJson getObjectRelatedFieldsDropdown(Long objectId) throws IOException {

        FieldDropdownJson fieldDropdownJson = new FieldDropdownJson();

        PortalObject object = getObjectById(objectId);
        fieldDropdownJson.setObjectFields(object.getFields());

        List<RelatedJson> relatedJsons = new ArrayList<>();
        List<Related> relateds = object.getRelateds();
        for (Related related : relateds) {
            RelatedJson relatedJson = getRelatedJson(related.getId());
            relatedJsons.add(relatedJson);
        }
        fieldDropdownJson.setRelatedFields(relatedJsons);

        return fieldDropdownJson;
    }

    public Pair<ObjectRelatedValueDB,ObjectRelatedValue> getObjectRelatedValuePair(Long objectId, Long relatedId, Long pk) {
        return objectCache.getObjectRelatedValuePair(objectId, relatedId, pk);
    }

    public ObjectRelatedValue getObjectRelatedValue(Long objectId, Long relatedId, Long pk) {
        return objectCache.getObjectRelatedValuePair(objectId, relatedId, pk).getValue();
    }

    public void saveObjectRelatedValue(ObjectRelatedValueDB db, ObjectRelatedValue value) throws JsonProcessingException {
        if( db.getId() != null ) {
            db.setLastUpdateDate(LocalDateTime.now());
            db.setLastUpdatedBy(userService.getUserName());

            objectCache.updateObjectValue(db,value);
        } else {
            db.setLastUpdateDate(LocalDateTime.now());
            db.setLastUpdatedBy(userService.getUserName());
            db.setCreationDate(LocalDateTime.now());
            db.setCreatedBy(userService.getUserName());

            objectCache.saveObjectValue(db,value);

            value.setId(db.getId());

            db.setJson(objectMapper.writeValueAsString(value));

            objectCache.updateObjectValue(db,value);
        }

    }

    public void populateNewSeqs(ObjectRelatedValue value) {
        List<DisplayFieldValue> newPkFields = value.getDisplayFieldValues().stream()
                .filter(v -> Field.PRIMARY_KEY_FIELD.equalsIgnoreCase(v.getMeta().getType()) && ScreenInstancePK.NEW_PK.equals(v.getValue().getValue()))
                .collect(Collectors.toList());
        for (DisplayFieldValue newPkField : newPkFields) {
            long nextSeqVal = objectCache.getNextSeqVal(newPkField.getObjectId(), newPkField.getRelatedId());
            newPkField.getValue().setValue(nextSeqVal);
        }
    }
}
