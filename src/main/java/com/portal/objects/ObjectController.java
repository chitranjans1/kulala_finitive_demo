package com.portal.objects;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.objects.domain.Field;
import com.portal.objects.dal.bean.RelatedDB;
import com.portal.common.exception.LogException;
import com.portal.common.exception.PortalRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

@RestController
public class ObjectController {

    @Autowired
    private ObjectService objectService;

    @Autowired
    private LogException logException;

    @Autowired
    private ObjectMapper objectMapper;

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/saveObject", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveObject(@RequestBody String objectJson) throws IOException {
        Long id = objectService.saveObject(objectJson);
        return new ResponseEntity<>("{\"success\":true, \"id\":" + id + "}", HttpStatus.CREATED);
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/saveObjectValue", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> saveObjectValue(@RequestBody String objectJson) {
        try {
            objectService.saveObject(objectJson);
            return new ResponseEntity<>(true, HttpStatus.CREATED);
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            return new ResponseEntity<>(false, HttpStatus.CREATED);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/getObjectsAndSubObjectsById/{objectId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getObjectsAndSubObjectsById(@PathVariable Long objectId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectService.getObjectAndSubObjectsById(objectId);
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/getRelatedObjectsById/{objectId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getRelatedObjectsById(@PathVariable Long objectId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectService.getRelatedObjectsById(objectId);
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/getObjectById/{objectId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getObjectById(@PathVariable Long objectId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectService.getObjectJsonById(objectId);
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/getFieldsByObjectId/{objectId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getFieldsByObjectId(@PathVariable Long objectId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectMapper.writeValueAsString(objectService.getFieldsByObjectId(objectId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/getFieldsByObjectIdRelatedId/{objectId}/{relatedObjectId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getFieldsByObjectIdRelatedId(@PathVariable Long objectId, @PathVariable(required = false) Long relatedObjectId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            Map<String, Field> fields = objectService.getFieldsByObjectIdRelatedId(objectId, relatedObjectId);
            return objectMapper.writeValueAsString(fields);
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/getAllObjects", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllObjects(@RequestParam(required = false,defaultValue = "list") String format) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            if( "list".equalsIgnoreCase(format) )
                return objectMapper.writeValueAsString(objectService.getObjectIdsAsList());
            else
                return objectMapper.writeValueAsString(objectService.getObjectIdsAsMap());
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/getAvailableObjects/{objectId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAvailableObjects(@PathVariable Long objectId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectMapper.writeValueAsString(objectService.getAvailableObjects(objectId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/getAllObjectsWithDef", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllObjectsWithDef() throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectMapper.writeValueAsString(objectService.getAllObjects());
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/deleteObject/{objectId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteObject(@PathVariable Long objectId) throws IOException {
        objectService.deleteObject(objectId);
        return new ResponseEntity<>("{\"success\":true}", HttpStatus.OK);

    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/related/saveRelated", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveRelated(@RequestBody String json) throws IOException {
        RelatedDB relatedDB = objectService.saveRelated(json);
        return new ResponseEntity<>("{\"success\":true, \"id\":" + relatedDB.getId() + "}", HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/related/{relatedId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getRelated(@PathVariable Long relatedId) throws IOException {
        return objectMapper.writeValueAsString(objectService.getRelatedJson(relatedId));
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/related", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllRelated(@RequestParam(required = false) Long objectId) throws IOException {
        return objectMapper.writeValueAsString(objectService.getAllRelated(objectId));
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/related/deleteRelated/{relatedId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteRelated(@PathVariable Long relatedId) throws IOException {
        objectService.deleteRelated(relatedId);
        return new ResponseEntity<>("{\"success\":true}", HttpStatus.OK);

    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/object/getFields/{objectId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getObjectOrRelatedFields(@PathVariable Long objectId) throws IOException {
        return objectMapper.writeValueAsString(objectService.getObjectRelatedFieldsDropdown(objectId));
    }
}
