package com.portal.displaygroup.domain;

import java.io.Serializable;

public class DisplayGroupIdJson implements Serializable {

    private DisplayGroup displayGroup;

    public DisplayGroupIdJson(DisplayGroup displayGroup) {
        this.displayGroup = displayGroup;
    }

    public Long getId() {
        return displayGroup.getId();
    }

    public String getName() {
        return displayGroup.getName();
    }

    public String getDisplayName() {
        return displayGroup.getDisplayName();
    }

    public String getType() {
        return displayGroup.getType();
    }
}