package com.portal.displaygroup.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class DisplayField implements Serializable {

    private Long objectId;
    private Long relatedId;
    private String fieldName;
    private String displayName;
    private String type;
    private String displayType;
}
