package com.portal.displaygroup.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DisplayGroup implements Serializable {

    private Long id;
    private List<DisplayFieldValue> displayFields = new ArrayList<>();
    private List<DisplayGroup> subDisplayGroups = new ArrayList<>();
    private String name;
    private String displayName;
    private String type;
}
