package com.portal.displaygroup.domain;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.portal.common.helper.JsonHelper;
import com.portal.objects.domain.Field;
import com.portal.objects.domain.FieldValueObject;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonFilter(JsonHelper.DISPLAY_FIELD_DEF_FILTER)
public class DisplayFieldValue extends DisplayField implements Serializable {
    private Field meta;
    private FieldValueObject value;
}
