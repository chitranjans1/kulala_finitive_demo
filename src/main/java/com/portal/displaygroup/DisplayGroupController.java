package com.portal.displaygroup;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.common.exception.LogException;
import com.portal.common.exception.PortalRuntimeException;
import com.portal.displaygroup.domain.DisplayGroup;
import com.portal.screens.ScreenService;
import com.portal.screens.domain.Screen;
import com.portal.screens.domain.ScreenInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

@RestController()
@RequestMapping("/rest/priv/displaygroup")
public class DisplayGroupController {

    @Autowired
    private DisplayGroupService displayGroupService;

    @Autowired
    private LogException logException;

    @Autowired
    private ObjectMapper objectMapper;

    @Transactional
    @CrossOrigin
    @RequestMapping(value = "/saveDisplayGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveDisplayGroup(@RequestBody String json) throws IOException {
        try {
            DisplayGroup displayGroup = displayGroupService.saveDisplayGroup(json);
            return new ResponseEntity<>("{\"success\":true, \"id\":" + displayGroup.getId() + "}", HttpStatus.CREATED);
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/getDisplayGroupById/{displayGroupId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getDisplayGroupById(@PathVariable Long displayGroupId) throws IOException {
        try {
            return objectMapper.writer(displayGroupService.getFilterProviderForScreenDef()).writeValueAsString(displayGroupService.getDisplayGroupById(displayGroupId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/getAllDisplayGroups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllDisplayGroups() throws IOException {
        try {
            return objectMapper.writeValueAsString(displayGroupService.getAllDisplayGroups());
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/getAllDisplayGroupsForScreen/{screenId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllDisplayGroupsForScreen(@PathVariable Long screenId) throws IOException {
        try {
            return objectMapper.writeValueAsString(displayGroupService.getAllDisplayGroupsForScreen(screenId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/getSubDisplayGroupsForDisplayGroup/{displayGroupId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getSubDisplayGroupsForDisplayGroup(@PathVariable Long displayGroupId) throws IOException {
        try {
            return objectMapper.writeValueAsString(displayGroupService.getSubDisplayGroupsForDisplayGroup(displayGroupId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/deleteDisplayGroup/{displayGroupId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteDisplayGroup(@PathVariable Long displayGroupId) throws IOException {
        displayGroupService.deleteDisplayGroup(displayGroupId);
        return new ResponseEntity<>("{\"success\":true}", HttpStatus.OK);
    }

//    @CrossOrigin
//    @RequestMapping(value = "/rest/priv/screen/getScreenInstanceById/{screenInstanceId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    public String getScreenInstanceById(@PathVariable Long screenInstanceId) throws IOException, InvocationTargetException, IllegalAccessException {
//        try {
//            return objectMapper.writeValueAsString(screenService.getScreenForScreenInstance(screenInstanceId));
//        } catch (Throwable t) {
//            logException.log(new PortalRuntimeException(t));
//            throw t;
//        }
//    }
//
//    @CrossOrigin
//    @RequestMapping(value = "/rest/priv/screen/getAllScreenInstanceByScreenId/{screenId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    public String getAllScreenInstanceByScreenId(@PathVariable Long screenId) throws IOException, InvocationTargetException, IllegalAccessException {
//        try {
//            return objectMapper.writeValueAsString(screenService.getAllScreenInstanceByScreenId(screenId));
//        } catch (Throwable t) {
//            logException.log(new PortalRuntimeException(t));
//            throw t;
//        }
//    }
//
//    @CrossOrigin
//    @RequestMapping(value = "/rest/priv/screen/getNewScreenInstanceForScreen/{screenId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    public String getNewScreenInstanceForScreen(@PathVariable Long screenId) throws IOException, InvocationTargetException, IllegalAccessException {
//        try {
//            return objectMapper.writeValueAsString(screenService.getNewScreenInstanceForScreen(screenId));
//        } catch (Throwable t) {
//            logException.log(new PortalRuntimeException(t));
//            throw t;
//        }
//    }
//
//    @Transactional
//    @CrossOrigin
//    @RequestMapping(value = "/rest/priv/screen/saveScreenInstance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<String> saveScreenInstance(@RequestBody String json) throws IOException {
//        try {
//            ScreenInstance screenInstance = screenService.saveScreenInstance(json);
//            return new ResponseEntity<>("{\"success\":true, \"id\":" + screenInstance.getId() + "}", HttpStatus.CREATED);
//        } catch (Throwable t) {
//            logException.log(new PortalRuntimeException(t));
//            throw t;
//        }
//    }
}
