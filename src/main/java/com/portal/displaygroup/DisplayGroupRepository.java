package com.portal.displaygroup;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.portal.common.dal.PortalDB;
import com.portal.common.helper.JDBCutil;
import com.portal.common.spring.PortalBeanPropertyRowMapper;
import com.portal.screens.dal.bean.DisplayGroupDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class DisplayGroupRepository {

    private static transient final Logger LOG = LoggerFactory.getLogger(DisplayGroupRepository.class);

    @Autowired
    @PortalDB
    private JdbcTemplate jdbcTemplate;

    public List<DisplayGroupDB> getDisplayGroups() {
        List<DisplayGroupDB> objects = jdbcTemplate.query("select * from display_groups",
                new PortalBeanPropertyRowMapper<>(DisplayGroupDB.class));
        return  objects;
    }

    public Long save(DisplayGroupDB displayGroupDB) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(displayGroupDB.getJson());
        paramsList.add(displayGroupDB.getCreatedBy());
        paramsList.add(displayGroupDB.getCreationDate());
        paramsList.add(displayGroupDB.getLastUpdatedBy());
        paramsList.add(displayGroupDB.getLastUpdateDate());

        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        StringBuilder sbSql = new StringBuilder("insert into display_groups (");
        sbSql.append("json,created_by,creation_date,last_updated_by,last_update_date")
                .append(") ")
                .append(JDBCutil.generateValuesPlaceHolderClause(paramArr.length))
                .append(" returning id");

        Long id = jdbcTemplate.queryForObject(sbSql.toString(),paramArr,Long.class);
        displayGroupDB.setId(id);
        return id;
    }

    public void update(DisplayGroupDB displayGroupDB) throws JsonProcessingException {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(displayGroupDB.getJson());
        paramsList.add(displayGroupDB.getLastUpdatedBy());
        paramsList.add(displayGroupDB.getLastUpdateDate());
        paramsList.add(displayGroupDB.getId());
        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        int count = jdbcTemplate.update(
                "update display_groups set json = ?, last_updated_by = ?, last_update_date = ? where id = ? ",
                paramArr);
        LOG.info("Updated "+count+" display groups");
    }

    public void delete(Long displayGroupId) {
        jdbcTemplate.update("delete from display_groups where id = ?", new Object[]{displayGroupId});
        LOG.info("Deleted "+displayGroupId+" from display groups");
    }
}
