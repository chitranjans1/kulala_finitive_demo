package com.portal.displaygroup;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.portal.screens.dal.bean.DisplayGroupDB;
import com.portal.displaygroup.domain.DisplayGroup;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DisplayGroupCache {

    @Autowired
    private DisplayGroupRepository displayGroupRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private HazelcastInstance hazelcastInstance;

    public static final String DISPLAY_GROUP_CACHE = "displayGroupCache";

    public void saveDisplayGroup(DisplayGroupDB displayGroupDB, DisplayGroup displayGroup) throws JsonProcessingException {
        displayGroupRepository.save(displayGroupDB);
        addToCache(displayGroupDB,displayGroup);
    }

    public void updateDisplayGroup(DisplayGroupDB displayGroupDB,DisplayGroup displayGroup) throws JsonProcessingException {
        displayGroupRepository.update(displayGroupDB);
        addToCache(displayGroupDB,displayGroup);
    }

    public void removeDisplayGroup(Long displayGroupId) {
        displayGroupRepository.delete(displayGroupId);
        IMap<Long, Pair<DisplayGroupDB,DisplayGroup>> cache = hazelcastInstance.getMap(DISPLAY_GROUP_CACHE);
        cache.remove(displayGroupId);
    }

    public Pair<DisplayGroupDB,DisplayGroup> getDisplayGroupPair(Long displayGroupId) {
        IMap<Long, Pair<DisplayGroupDB,DisplayGroup>> cache = hazelcastInstance.getMap(DISPLAY_GROUP_CACHE);
        return cache.get(displayGroupId);
    }

    public DisplayGroupDB getDisplayGroupDB(Long displayGroupId) {
        return getDisplayGroupPair(displayGroupId).getKey();
    }

    public DisplayGroup getDisplayGroup(Long displayGroupId) {
        return getDisplayGroupPair(displayGroupId).getValue();
    }

    @PostConstruct
    public void loadScreenCache(){
        List<DisplayGroupDB> displayGroupDBs = displayGroupRepository.getDisplayGroups();
        List<DisplayGroup> displayGroupsWithSubDisplays = new ArrayList<>();
        for (DisplayGroupDB displayGroupDB : displayGroupDBs) {
            try {
                DisplayGroup displayGroup = objectMapper.readValue(displayGroupDB.getJson(), DisplayGroup.class);
                addToCache(displayGroupDB,displayGroup);
                if(!displayGroup.getSubDisplayGroups().isEmpty())
                    displayGroupsWithSubDisplays.add(displayGroup);
            } catch (IOException e) {
                throw new RuntimeException("unable to convert json for display group "+displayGroupDB.getId());
            }
        }

        for (DisplayGroup displayGroup : displayGroupsWithSubDisplays) {
            List<DisplayGroup> newSubDisplayGroups = displayGroup.getSubDisplayGroups().stream()
                    .map(dg -> getDisplayGroup(dg.getId()))
                    .collect(Collectors.toList());

            displayGroup.setSubDisplayGroups(newSubDisplayGroups);
        }

    }

    private void addToCache(DisplayGroupDB displayGroupDB, DisplayGroup displayGroup) {
        //addOneElementAsObject(DISPLAY_GROUP_CACHE, displayGroupDB.getId(), Pair.of(displayGroupDB,displayGroup));
        IMap<Long, Pair<DisplayGroupDB,DisplayGroup>> cache = hazelcastInstance.getMap(DISPLAY_GROUP_CACHE);
        cache.put(displayGroupDB.getId(),Pair.of(displayGroupDB,displayGroup));
    }

    public List<DisplayGroup> getAllDisplayGroups() {
        IMap<Long, Pair<DisplayGroupDB,DisplayGroup>> cache = hazelcastInstance.getMap(DISPLAY_GROUP_CACHE);
        Set<Long> keySet = cache.keySet();

        List<DisplayGroup> list = new ArrayList<>();
        for (Long key : keySet) {
            Pair<DisplayGroupDB, DisplayGroup> pair = cache.get(key);
            list.add(pair.getValue());
        }

        return list;
    }
}
