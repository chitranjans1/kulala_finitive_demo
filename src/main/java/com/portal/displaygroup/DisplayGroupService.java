package com.portal.displaygroup;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.portal.common.domain.IdNameBean;
import com.portal.common.exception.ValidationException;
import com.portal.common.helper.JsonHelper;
import com.portal.displaygroup.domain.DisplayGroup;
import com.portal.displaygroup.domain.DisplayGroupIdJson;
import com.portal.screens.ScreenService;
import com.portal.screens.dal.bean.DisplayGroupDB;
import com.portal.screens.domain.Screen;
import com.portal.security.access.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DisplayGroupService {
    private static final Log LOG = LogFactory.getLog(DisplayGroupService.class);

    @Autowired
    private DisplayGroupCache displayGroupCache;

    @Autowired
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JsonHelper jsonHelper;

    @Autowired
    private ScreenService screenService;


    public DisplayGroup saveDisplayGroup(String json) throws IOException {
        DisplayGroup displayGroup = objectMapper.readValue(json, DisplayGroup.class);

        if(StringUtils.isBlank(displayGroup.getDisplayName())) {
            displayGroup.setDisplayName(displayGroup.getName());
            json = objectMapper.writeValueAsString(displayGroup);
        }

        DisplayGroupDB displayGroupDB;
        if( displayGroup.getId() != null )
        {
            displayGroupDB = new DisplayGroupDB();
            displayGroupDB.setId(displayGroup.getId());
            displayGroupDB.setLastUpdateDate(LocalDateTime.now());
            displayGroupDB.setLastUpdatedBy(userService.getUserName());
            displayGroupDB.setJson(json);
            displayGroupDB.setName(displayGroup.getName());
            displayGroupCache.updateDisplayGroup(displayGroupDB,displayGroup);
        }
        else
        {
            boolean displayGroupExists = getAllDisplayGroups().stream()
                    .filter(dg -> dg.getName().equalsIgnoreCase(displayGroup.getName()))
                    .findAny().isPresent();
            if( displayGroupExists )
                throw new ValidationException("Display group with name "+displayGroup.getName()+" already exists");

            displayGroupDB = new DisplayGroupDB();
            displayGroupDB.setLastUpdateDate(LocalDateTime.now());
            displayGroupDB.setLastUpdatedBy(userService.getUserName());
            displayGroupDB.setCreationDate(LocalDateTime.now());
            displayGroupDB.setCreatedBy(userService.getUserName());
            displayGroupDB.setJson(json);
            displayGroupDB.setName(displayGroup.getName());
            displayGroupCache.saveDisplayGroup(displayGroupDB,displayGroup);

            displayGroup.setId(displayGroupDB.getId());
        }

        ObjectNode jsonNode = (ObjectNode)objectMapper.readTree(json);

        jsonNode.put("id",displayGroup.getId());
        displayGroupDB.setJson(objectMapper.writeValueAsString(jsonNode));
        displayGroupCache.updateDisplayGroup(displayGroupDB,displayGroup);

        return displayGroup;
    }

    public List<DisplayGroupIdJson> getAllDisplayGroups() {
        return displayGroupCache.getAllDisplayGroups().stream()
                .map(dg -> new DisplayGroupIdJson(dg))
                .collect(Collectors.toList());
    }

    public List<DisplayGroupIdJson> getAllDisplayGroupsForScreen(Long screenId) throws IOException {
        Screen screen = screenService.getScreenObjById(screenId);
        Set<Long> displayGroupIdsAlreadyOnScreen = screen.getDisplayGroups().stream().map(dg -> dg.getId()).collect(Collectors.toSet());

        return displayGroupCache.getAllDisplayGroups().stream()
                .filter(dg -> !displayGroupIdsAlreadyOnScreen.contains(dg.getId()) )
                .map(dg -> new DisplayGroupIdJson(dg))
                .collect(Collectors.toList());
    }

    public List<DisplayGroupIdJson> getSubDisplayGroupsForDisplayGroup(Long displayGroupId) throws IOException {
        DisplayGroup displayGroup = displayGroupCache.getDisplayGroup(displayGroupId);

        Set<Long> subDisplayGroupIdsAlreadyThere = displayGroup.getSubDisplayGroups().stream().map(dg -> dg.getId()).collect(Collectors.toSet());

        return displayGroupCache.getAllDisplayGroups().stream()
                .filter(dg -> !displayGroup.getId().equals(dg.getId()) && dg.getSubDisplayGroups().isEmpty() && !subDisplayGroupIdsAlreadyThere.contains(dg.getId()) )
                .map(dg -> new DisplayGroupIdJson(dg))
                .collect(Collectors.toList());
    }

    public DisplayGroup getDisplayGroupById(Long displayGroupId) {
        DisplayGroup displayGroup = displayGroupCache.getDisplayGroup(displayGroupId);
        ListIterator<DisplayGroup> itr = displayGroup.getSubDisplayGroups().listIterator();
        while(itr.hasNext()) {
            DisplayGroup subDisplayGroup = itr.next();
            DisplayGroup subDisplayGroupFromCache = displayGroupCache.getDisplayGroup(subDisplayGroup.getId());
            itr.set(subDisplayGroupFromCache);
        }
        return displayGroup;
    }

    public FilterProvider getFilterProviderForScreenDef() {
        return jsonHelper.getFilterProvider(
                jsonHelper.getDisplayFieldDefinitionFilterPair()
        );
    }

    public void deleteDisplayGroup(Long displayGroupId) throws IOException {
        DisplayGroup displayGroup = displayGroupCache.getDisplayGroup(displayGroupId);

        List<IdNameBean> screenIdsAsList = screenService.getScreenIdsAsList();
        for (IdNameBean idNameBean : screenIdsAsList) {
            Screen screen = screenService.getScreenObjById(idNameBean.getId());

            boolean used = screen.getDisplayGroups().stream()
                    .filter(dg -> dg.getId().equals(displayGroup.getId()))
                    .findAny().isPresent();

            if( used )
                throw new ValidationException("Display group is used in screen "+screen.getName());
        }

        displayGroupCache.removeDisplayGroup(displayGroupId);

    }
}

