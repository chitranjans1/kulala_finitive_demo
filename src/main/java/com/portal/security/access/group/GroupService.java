package com.portal.security.access.group;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.screens.dal.bean.GroupDB;
import com.portal.security.access.group.domain.Group;
import com.portal.security.access.user.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class GroupService {
    private static final Log LOG = LogFactory.getLog(GroupService.class);

    @Autowired
    private GroupCache groupCache;

    @Autowired
    private GroupService groupService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserService userService;

    public Group saveGroup(String json) throws IOException {
        Group group = objectMapper.readValue(json, Group.class);

        GroupDB groupDB;
        if( group.getId() == null )
        {
            groupDB = new GroupDB();
            groupDB.setId(group.getId());
            groupDB.setLastUpdateDate(LocalDateTime.now());
            groupDB.setLastUpdatedBy(userService.getUserName());
            groupDB.setJson(json);
            group = groupCache.saveGroup(groupDB, group);
        }
        else
        {
            groupDB = new GroupDB();
            groupDB.setId(group.getId());
            groupDB.setLastUpdateDate(LocalDateTime.now());
            groupDB.setLastUpdatedBy(userService.getUserName());
            groupDB.setCreationDate(LocalDateTime.now());
            groupDB.setCreatedBy(userService.getUserName());
            groupDB.setJson(json);
            groupDB.setId(group.getId());
            groupCache.updateGroup(groupDB, group);
        }
        return group;
    }

    public List<Group> getAllGroups() {
        return new ArrayList<>(groupCache.getAllGroups());
    }

    public void deleteGroup(Long id) throws IOException {
        groupCache.removeGroup(id);
    }

    public Group getGroupById(Long id) {
        return groupCache.getGroup(id);
    }

}

