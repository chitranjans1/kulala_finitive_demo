package com.portal.security.access.group;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.common.exception.LogException;
import com.portal.common.exception.PortalRuntimeException;
import com.portal.security.access.group.domain.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController()
@RequestMapping("/rest/priv/group")
public class GroupController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private LogException logException;

    @Autowired
    private ObjectMapper objectMapper;

    @Transactional
    @CrossOrigin
    @RequestMapping(value = "/saveGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveGroup(@RequestBody String json) throws IOException {
        try {
            Group group = groupService.saveGroup(json);
            return new ResponseEntity<>("{\"success\":true, \"id\":" + group.getId() + "}", HttpStatus.CREATED);
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/getAllGroups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllGroups() throws IOException {
        try {
            return objectMapper.writeValueAsString(groupService.getAllGroups());
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/deleteGroup/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteDisplayGroup(@PathVariable Long id) throws IOException {
        groupService.deleteGroup(id);
        return new ResponseEntity<>("{\"success\":true}", HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/getGroupById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getGroupById(@PathVariable Long id) throws IOException {
        try {
            return objectMapper.writeValueAsString(groupService.getGroupById(id));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

}
