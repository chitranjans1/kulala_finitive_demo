package com.portal.security.access.group.domain;

import com.portal.security.access.roles.domain.Role;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Group implements Serializable {

    private Long id;
    private String name;
    private List<Long> roleIds;
}
