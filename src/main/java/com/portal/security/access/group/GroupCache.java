package com.portal.security.access.group;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.portal.screens.dal.bean.GroupDB;
import com.portal.security.access.group.domain.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

@Service
public class GroupCache {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private HazelcastInstance hazelcastInstance;

    public static final String GROUP_CACHE = "groupCache";

    public Group saveGroup(GroupDB groupDB, Group group) throws JsonProcessingException {
        Long id = groupRepository.save(groupDB);
        group.setId(id);
        String json  = objectMapper.writeValueAsString(group);
        groupDB.setJson(json);
        groupRepository.update(groupDB);
        addToCache(group);
        return group;
    }

    public void updateGroup(GroupDB groupDB, Group group) throws JsonProcessingException {
        groupRepository.update(groupDB);
        addToCache(group);
    }

    public void removeGroup(Long id) {
        groupRepository.delete(id);
        IMap<Long, Group> cache = hazelcastInstance.getMap(GROUP_CACHE);
        cache.remove(id);
    }

    public Group getGroup(Long id) {
        IMap<Long, Group> cache = hazelcastInstance.getMap(GROUP_CACHE);
        return cache.get(id);
    }

   @PostConstruct
    public void loadGroups() throws IOException {
        List<GroupDB> groups = groupRepository.getAllGroups();
        IMap<Long, Group> cache = hazelcastInstance.getMap(GROUP_CACHE);
        for (GroupDB groupDB : groups) {
            Group group = objectMapper.readValue(groupDB.getJson(), Group.class);
            cache.put(group.getId(), group);
        }
    }

    private Group addToCache(Group group) {
        IMap<Long, Group> cache = hazelcastInstance.getMap(GROUP_CACHE);
        cache.put(group.getId(), group);
        return group;
    }

    public Collection<Group> getAllGroups() {
        IMap<Long, Group> cache = hazelcastInstance.getMap(GROUP_CACHE);
        return cache.values();
    }
}
