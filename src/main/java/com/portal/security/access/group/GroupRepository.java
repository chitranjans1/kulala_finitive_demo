package com.portal.security.access.group;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.portal.common.dal.PortalDB;
import com.portal.common.helper.JDBCutil;
import com.portal.common.spring.PortalBeanPropertyRowMapper;
import com.portal.screens.dal.bean.GroupDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class GroupRepository {

    private static transient final Logger LOG = LoggerFactory.getLogger(GroupRepository.class);

    @Autowired
    @PortalDB
    private JdbcTemplate jdbcTemplate;

    public List<GroupDB> getAllGroups() {
        List<GroupDB> groups = jdbcTemplate.query("select * from groups",
                new PortalBeanPropertyRowMapper<>(GroupDB.class));
        return groups;
    }

    public Long save(GroupDB groupDB) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(groupDB.getJson());
        paramsList.add(groupDB.getCreatedBy());
        paramsList.add(groupDB.getCreationDate());
        paramsList.add(groupDB.getLastUpdatedBy());
        paramsList.add(groupDB.getLastUpdateDate());

        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        StringBuilder sbSql = new StringBuilder("insert into groups (");
        sbSql.append("json,created_by,creation_date,last_updated_by,last_update_date")
                .append(") ")
                .append(JDBCutil.generateValuesPlaceHolderClause(paramArr.length))
                .append(" returning id");

        Long id = jdbcTemplate.queryForObject(sbSql.toString(), paramArr, Long.class);
        groupDB.setId(id);
        return id;
    }

    public void update (GroupDB groupDB) throws JsonProcessingException {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(groupDB.getJson());
        paramsList.add(groupDB.getLastUpdatedBy());
        paramsList.add(groupDB.getLastUpdateDate());
        paramsList.add(groupDB.getId());
        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        int count = jdbcTemplate.update(
                "update groups set json = ?, last_updated_by = ?, last_update_date = ? where id = ? ",
                paramArr);
        LOG.info("Updated " + count + " groups");
    }

    public void delete(Long id) {
        jdbcTemplate.update("delete from groups where id = ?", new Object[]{id});
        LOG.info("Deleted " + id + " from groups");
    }
}
