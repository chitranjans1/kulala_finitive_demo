package com.portal.security.access.entitlements.object;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.portal.screens.dal.bean.ObjectEntitlementDB;
import com.portal.security.access.entitlements.object.domain.ObjectEntitlement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

@Service
public class ObjectEntitlementCache {

    @Autowired
    private ObjectEntitlementRepository objectEntitlementRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private HazelcastInstance hazelcastInstance;

    public static final String OBJECT_ENTITLEMENT_CACHE = "objectEntitlementCache";

    public ObjectEntitlement saveObjectEntitlement(ObjectEntitlementDB objectEntitlementDB, ObjectEntitlement objectEntitlement) throws JsonProcessingException {
        Long objectEntitlementId = objectEntitlementRepository.save(objectEntitlementDB);
        objectEntitlement.setId(objectEntitlementId);
        String json  = objectMapper.writeValueAsString(objectEntitlement);
        objectEntitlementDB.setJson(json);
        objectEntitlementRepository.update(objectEntitlementDB);
        addToCache(objectEntitlement);
        return objectEntitlement;
    }

    public void updateObjectEntitlement(ObjectEntitlementDB objectEntitlementDB, ObjectEntitlement objectEntitlement) throws JsonProcessingException {
        objectEntitlementRepository.update(objectEntitlementDB);
        addToCache(objectEntitlement);
    }

    public void removeObjectEntitlement(Long objectEntitlementId) {
        objectEntitlementRepository.delete(objectEntitlementId);
        IMap<Long, ObjectEntitlement> cache = hazelcastInstance.getMap(OBJECT_ENTITLEMENT_CACHE);
        cache.remove(objectEntitlementId);
    }

    public ObjectEntitlement getObjectEntitlement(Long objectEntitlementId) {
        IMap<Long, ObjectEntitlement> cache = hazelcastInstance.getMap(OBJECT_ENTITLEMENT_CACHE);
        return cache.get(objectEntitlementId);
    }

   @PostConstruct
    public void loadObjectEntitlements() throws IOException {
        List<ObjectEntitlementDB> objectEntitlements = objectEntitlementRepository.getAllObjectEntitlements();
        IMap<Long, ObjectEntitlement> cache = hazelcastInstance.getMap(OBJECT_ENTITLEMENT_CACHE);
        for (ObjectEntitlementDB objectEntitlementDB : objectEntitlements) {
            ObjectEntitlement objectEntitlement = objectMapper.readValue(objectEntitlementDB.getJson(), ObjectEntitlement.class);
            cache.put(objectEntitlement.getId(), objectEntitlement);
        }
    }

    private ObjectEntitlement addToCache(ObjectEntitlement objectEntitlement) {
        IMap<Long, ObjectEntitlement> cache = hazelcastInstance.getMap(OBJECT_ENTITLEMENT_CACHE);
        cache.put(objectEntitlement.getId(), objectEntitlement);
        return objectEntitlement;
    }

    public Collection<ObjectEntitlement> getAllObjectEntitlements() {
        IMap<Long, ObjectEntitlement> cache = hazelcastInstance.getMap(OBJECT_ENTITLEMENT_CACHE);
        return cache.values();
    }
}
