package com.portal.security.access.entitlements.object.domain;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Entitlement implements Serializable {

    private boolean isCreateAllowed;
    private String editEntitlement;
    private String fieldName;
    private String objectName;
    private Long objectId;
    private String relatedName;
}
