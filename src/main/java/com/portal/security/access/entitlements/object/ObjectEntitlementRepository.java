package com.portal.security.access.entitlements.object;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.portal.common.dal.PortalDB;
import com.portal.common.helper.JDBCutil;
import com.portal.common.spring.PortalBeanPropertyRowMapper;
import com.portal.screens.dal.bean.ObjectEntitlementDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ObjectEntitlementRepository {

    private static transient final Logger LOG = LoggerFactory.getLogger(ObjectEntitlementRepository.class);

    @Autowired
    @PortalDB
    private JdbcTemplate jdbcTemplate;

    public List<ObjectEntitlementDB> getAllObjectEntitlements() {
        List<ObjectEntitlementDB> object_entitlements = jdbcTemplate.query("select * from object_entitlements",
                new PortalBeanPropertyRowMapper<>(ObjectEntitlementDB.class));
        return object_entitlements;
    }

    public Long save(ObjectEntitlementDB objectEntitlementDB) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(objectEntitlementDB.getJson());
        paramsList.add(objectEntitlementDB.getCreatedBy());
        paramsList.add(objectEntitlementDB.getCreationDate());
        paramsList.add(objectEntitlementDB.getLastUpdatedBy());
        paramsList.add(objectEntitlementDB.getLastUpdateDate());

        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        StringBuilder sbSql = new StringBuilder("insert into object_entitlements (");
        sbSql.append("json,created_by,creation_date,last_updated_by,last_update_date")
                .append(") ")
                .append(JDBCutil.generateValuesPlaceHolderClause(paramArr.length))
                .append(" returning id");

        Long id = jdbcTemplate.queryForObject(sbSql.toString(), paramArr, Long.class);
        objectEntitlementDB.setId(id);
        return id;
    }

    public void update (ObjectEntitlementDB objectEntitlementDB) throws JsonProcessingException {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(objectEntitlementDB.getJson());
        paramsList.add(objectEntitlementDB.getLastUpdatedBy());
        paramsList.add(objectEntitlementDB.getLastUpdateDate());
        paramsList.add(objectEntitlementDB.getId());
        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        int count = jdbcTemplate.update(
                "update object_entitlements set json = ?, last_updated_by = ?, last_update_date = ? where id = ? ",
                paramArr);
        LOG.info("Updated " + count + " object_entitlements");
    }

    public void delete(Long objectEntitlementId) {
        jdbcTemplate.update("delete from object_entitlements where id = ?", new Object[]{objectEntitlementId});
        LOG.info("Deleted " + objectEntitlementId + " from object_entitlements");
    }
}
