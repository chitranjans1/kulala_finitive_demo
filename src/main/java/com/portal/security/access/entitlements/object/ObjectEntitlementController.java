package com.portal.security.access.entitlements.object;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.common.exception.LogException;
import com.portal.common.exception.PortalRuntimeException;
import com.portal.security.access.entitlements.object.domain.ObjectEntitlement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Optional;

@RestController()
@RequestMapping("/rest/priv/objectEntitlement")
public class ObjectEntitlementController {

    @Autowired
    private ObjectEntitlementService objectEntitlementService;

    @Autowired
    private LogException logException;

    @Autowired
    private ObjectMapper objectMapper;

    @Transactional
    @CrossOrigin
    @RequestMapping(value = "/saveObjectEntitlement", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveObjectEntitlement(@RequestBody String json) throws IOException {
        try {
            ObjectEntitlement objectEntitlement = objectEntitlementService.saveObjectEntitlement(json);
            return new ResponseEntity<>("{\"success\":true, \"id\":" + objectEntitlement.getId() + "}", HttpStatus.CREATED);
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/getAllObjectEntitlements", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllObjectEntitlements() throws IOException {
        try {
            return objectMapper.writeValueAsString(objectEntitlementService.getAllObjectEntitlements());
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/deleteObjectEntitlement/{objectEntitlementId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteDisplayObjectEntitlement(@PathVariable Long objectEntitlementId) throws IOException {
        objectEntitlementService.deleteObjectEntitlement(objectEntitlementId);
        return new ResponseEntity<>("{\"success\":true}", HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = {"/getObjectEntitlementById/{id}", "/getObjectEntitlementById/{id}/{objectId}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getObjectEntitlementById(@PathVariable Long id, @PathVariable Optional<Long> objectId) throws IOException {
        try {
            return objectMapper.writeValueAsString(objectEntitlementService.getObjectEntitlementById(id, objectId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value = {"/getObjectEntitlement", "/getObjectEntitlement/{objectId}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getObjectEntitlement(@PathVariable Optional<Long> objectId) throws IOException {
        try {
            return objectMapper.writeValueAsString(objectEntitlementService.getObjectEntitlement(objectId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

}
