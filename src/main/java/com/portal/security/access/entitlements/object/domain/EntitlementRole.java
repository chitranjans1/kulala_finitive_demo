package com.portal.security.access.entitlements.object.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EntitlementRole implements Serializable {

    private String roleName;
    private boolean isSelected;


}
