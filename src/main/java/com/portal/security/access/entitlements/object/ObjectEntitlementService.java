package com.portal.security.access.entitlements.object;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.objects.ObjectService;
import com.portal.objects.domain.Field;
import com.portal.objects.domain.PortalObject;
import com.portal.objects.domain.Related;
import com.portal.screens.dal.bean.ObjectEntitlementDB;
import com.portal.security.access.entitlements.object.domain.Entitlement;
import com.portal.security.access.entitlements.object.domain.EntitlementRole;
import com.portal.security.access.entitlements.object.domain.ObjectEntitlement;
import com.portal.security.access.roles.RoleService;
import com.portal.security.access.roles.domain.Role;
import com.portal.security.access.user.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class ObjectEntitlementService {
    private static final Log LOG = LogFactory.getLog(ObjectEntitlementService.class);

    @Autowired
    private ObjectEntitlementCache objectEntitlementCache;

    @Autowired
    private ObjectService objectService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    private static String EDIT_ENTITLEMENT_NONE = "NONE";

    public ObjectEntitlement saveObjectEntitlement(String json) throws IOException {
        ObjectEntitlement group = objectMapper.readValue(json, ObjectEntitlement.class);

        ObjectEntitlementDB objectEntitlementDB;
        if( group.getId() == null )
        {
            objectEntitlementDB = new ObjectEntitlementDB();
            objectEntitlementDB.setId(group.getId());
            objectEntitlementDB.setLastUpdateDate(LocalDateTime.now());
            objectEntitlementDB.setLastUpdatedBy(userService.getUserName());
            objectEntitlementDB.setJson(json);
            group = objectEntitlementCache.saveObjectEntitlement(objectEntitlementDB, group);
        }
        else
        {
            objectEntitlementDB = new ObjectEntitlementDB();
            objectEntitlementDB.setId(group.getId());
            objectEntitlementDB.setLastUpdateDate(LocalDateTime.now());
            objectEntitlementDB.setLastUpdatedBy(userService.getUserName());
            objectEntitlementDB.setCreationDate(LocalDateTime.now());
            objectEntitlementDB.setCreatedBy(userService.getUserName());
            objectEntitlementDB.setJson(json);
            objectEntitlementDB.setId(group.getId());
            objectEntitlementCache.updateObjectEntitlement(objectEntitlementDB, group);
        }
        return group;
    }

    public List<ObjectEntitlement> getAllObjectEntitlements() {
        return new ArrayList<>(objectEntitlementCache.getAllObjectEntitlements());
    }

    public void deleteObjectEntitlement(Long id) throws IOException {
        objectEntitlementCache.removeObjectEntitlement(id);
    }

    public ObjectEntitlement getObjectEntitlementById(Long id, Optional<Long> objectId) {
        ObjectEntitlement objectEntitlement = null;
        if (objectId.isPresent())
        {
            ObjectEntitlement cacheObjectEntitlement = objectEntitlementCache.getObjectEntitlement(id);
            objectEntitlement = new ObjectEntitlement();
            objectEntitlement.setRoles(cacheObjectEntitlement.getRoles());
            objectEntitlement.setId(cacheObjectEntitlement.getId());
            objectEntitlement.setName(cacheObjectEntitlement.getName());
            List<Entitlement> editEntitlements = new ArrayList<>();
            objectEntitlement.setEntitlement(editEntitlements);
            for (Entitlement entitlement : objectEntitlement.getEntitlement())
            {
                if (entitlement.getObjectId().equals(objectId))
                {
                    editEntitlements.add(entitlement);
                }
            }
        }
        else {
            objectEntitlement = objectEntitlementCache.getObjectEntitlement(id);
        }
        return objectEntitlement;
    }

    public ObjectEntitlement getObjectEntitlement(Optional<Long> objectId) throws IOException {
        ObjectEntitlement objectEntitlement = new ObjectEntitlement();
        List<Role> roles = roleService.getAllRoles();
        List<EntitlementRole> entitlementRoles = new ArrayList<>();
        for (Role role : roles)
        {
            EntitlementRole entitlementRole = new EntitlementRole();
            entitlementRole.setRoleName(role.getName());
            entitlementRole.setSelected(false);
            entitlementRoles.add(entitlementRole);
        }
        objectEntitlement.setRoles(entitlementRoles);
        List<Entitlement> entitlements = new ArrayList<>();

        if (objectId.isPresent())
        {
            PortalObject object = objectService.getObjectById(objectId.get());
            entitlements.addAll(getEntitlements(object));
        }
        else {
            List<PortalObject> objects = objectService.getAllObjects();
            for (PortalObject object : objects) {
                entitlements.addAll(getEntitlements(object));
            }
        }
        objectEntitlement.setEntitlement(entitlements);
        return objectEntitlement;
    }

    private List<Entitlement> getEntitlements(PortalObject object) {
        List<Entitlement> objectEntitlements = getEntitlementForFields(object.getId(), object.getName(), null, object.getFields());
        List<Entitlement> relatedEntitlements = getEntitlementForRelateds(object);
        objectEntitlements.addAll(relatedEntitlements);
        return objectEntitlements;
    }

    private List<Entitlement> getEntitlementForRelateds(PortalObject object) {
        List<Entitlement> entitlements = new ArrayList<>();
        for (Related related : object.getRelateds())
        {
            List<Entitlement> relatedEntitlements = getEntitlementForFields(object.getId(), object.getName(), related.getObject().getName(), related.getRelatedFields());
            entitlements.addAll(relatedEntitlements);
        }
        return entitlements;

    }

    private List<Entitlement> getEntitlementForFields(Long objectId, String objectName, String relatedName, List<Field> fields) {
        List<Entitlement> entitlements = new ArrayList<>();
        for (Field field : fields)
        {
            Entitlement entitlement = new Entitlement();
            entitlement.setObjectName(objectName);
            entitlement.setRelatedName(relatedName);
            entitlement.setFieldName(field.getName());
            entitlement.setCreateAllowed(false);
            entitlement.setEditEntitlement(EDIT_ENTITLEMENT_NONE);
            entitlement.setObjectId(objectId);
            entitlements.add(entitlement);
        }
        return entitlements;
    }
}

