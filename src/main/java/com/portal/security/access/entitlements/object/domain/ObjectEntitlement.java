package com.portal.security.access.entitlements.object.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class ObjectEntitlement implements Serializable {

    private Long id;
    private String name;
    private List<Entitlement> entitlement;
    private List<EntitlementRole> roles;
}
