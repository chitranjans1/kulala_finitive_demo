package com.portal.security.access.user.domain;

import com.portal.security.access.group.domain.Group;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class User implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private List<Long> groupIds;
}
