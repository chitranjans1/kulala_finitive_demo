package com.portal.security.access.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.portal.screens.dal.bean.UserDB;
import com.portal.security.access.user.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

@Service
public class UserCache {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private HazelcastInstance hazelcastInstance;

    public static final String USER_CACHE = "userCache";

    public User saveUser(UserDB userDB, User user) throws JsonProcessingException {
        Long userId = userRepository.save(userDB);
        user.setId(userId);
        String json  = objectMapper.writeValueAsString(user);
        userDB.setJson(json);
        userRepository.update(userDB);
        addToCache(user);
        return user;
    }

    public void updateUser(UserDB userDB, User user) throws JsonProcessingException {
        userRepository.update(userDB);
        addToCache(user);
    }

    public void removeUser(Long userId) {
        userRepository.delete(userId);
        IMap<Long, User> cache = hazelcastInstance.getMap(USER_CACHE);
        cache.remove(userId);
    }

    public User getUser(Long userId) {
        IMap<Long, User> cache = hazelcastInstance.getMap(USER_CACHE);
        return cache.get(userId);
    }

    @PostConstruct
    public void loadUsers() throws IOException {
        List<UserDB> users = userRepository.getAllUsers();
        IMap<Long, User> cache = hazelcastInstance.getMap(USER_CACHE);
        for (UserDB userDB : users) {
            User user = objectMapper.readValue(userDB.getJson(), User.class);
            cache.put(user.getId(), user);
        }
    }

    private User addToCache(User user) {
        IMap<Long, User> cache = hazelcastInstance.getMap(USER_CACHE);
        cache.put(user.getId(), user);
        return user;
    }

    public Collection<User> getAllUsers() {
        IMap<Long, User> cache = hazelcastInstance.getMap(USER_CACHE);
        return cache.values();
    }
}
