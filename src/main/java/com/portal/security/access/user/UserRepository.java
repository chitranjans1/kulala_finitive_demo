package com.portal.security.access.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.portal.common.dal.PortalDB;
import com.portal.common.helper.JDBCutil;
import com.portal.common.spring.PortalBeanPropertyRowMapper;
import com.portal.screens.dal.bean.UserDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository {

    private static transient final Logger LOG = LoggerFactory.getLogger(UserRepository.class);

    @Autowired
    @PortalDB
    private JdbcTemplate jdbcTemplate;

    public List<UserDB> getAllUsers() {
        List<UserDB> users = jdbcTemplate.query("select * from users",
                new PortalBeanPropertyRowMapper<>(UserDB.class));
        return users;
    }

    public Long save(UserDB userDB) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(userDB.getJson());
        paramsList.add(userDB.getCreatedBy());
        paramsList.add(userDB.getCreationDate());
        paramsList.add(userDB.getLastUpdatedBy());
        paramsList.add(userDB.getLastUpdateDate());

        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        StringBuilder sbSql = new StringBuilder("insert into users (");
        sbSql.append("json,created_by,creation_date,last_updated_by,last_update_date")
                .append(") ")
                .append(JDBCutil.generateValuesPlaceHolderClause(paramArr.length))
                .append(" returning id");

        Long id = jdbcTemplate.queryForObject(sbSql.toString(), paramArr, Long.class);
        userDB.setId(id);
        return id;
    }

    public void update (UserDB userDB) throws JsonProcessingException {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(userDB.getJson());
        paramsList.add(userDB.getLastUpdatedBy());
        paramsList.add(userDB.getLastUpdateDate());
        paramsList.add(userDB.getId());
        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        int count = jdbcTemplate.update(
                "update users set json = ?, last_updated_by = ?, last_update_date = ? where id = ? ",
                paramArr);
        LOG.info("Updated " + count + " users");
    }

    public void delete(Long userId) {
        jdbcTemplate.update("delete from users where id = ?", new Object[]{userId});
        LOG.info("Deleted " + userId + " from users");
    }
}
