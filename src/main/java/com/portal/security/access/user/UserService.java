package com.portal.security.access.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.screens.dal.bean.UserDB;
import com.portal.security.access.user.domain.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    private static final Log LOG = LogFactory.getLog(UserService.class);

    @Autowired
    private UserCache userCache;

    @Autowired
    private ObjectMapper objectMapper;

    public User saveUser(String json) throws IOException {
        User user = objectMapper.readValue(json, User.class);

        UserDB userDB;
        if( user.getId() == null )
        {
            userDB = new UserDB();
            userDB.setId(user.getId());
            userDB.setLastUpdateDate(LocalDateTime.now());
            userDB.setLastUpdatedBy(getUserName());
            userDB.setJson(json);
            user = userCache.saveUser(userDB, user);
        }
        else
        {
            userDB = new UserDB();
            userDB.setId(user.getId());
            userDB.setLastUpdateDate(LocalDateTime.now());
            userDB.setLastUpdatedBy(getUserName());
            userDB.setCreationDate(LocalDateTime.now());
            userDB.setCreatedBy(getUserName());
            userDB.setJson(json);
            userDB.setId(user.getId());
            userCache.updateUser(userDB, user);
        }
        return user;
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(userCache.getAllUsers());
    }

    public User getUserById(Long id) {
        return userCache.getUser(id);
    }

    public void deleteUser(Long userId) throws IOException {
        userCache.removeUser(userId);
    }

    public String getUserName() {
//        if(!StringUtils.equalsIgnoreCase("prod", env.getProperty("deploy.env")))
//        {
//            return "chit@roccapital.com";
//        }
        return  (String) SecurityUtils.getSubject().getPrincipal();

    }
}

