package com.portal.security.access.roles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.portal.common.dal.PortalDB;
import com.portal.common.helper.JDBCutil;
import com.portal.common.spring.PortalBeanPropertyRowMapper;
import com.portal.screens.dal.bean.RoleDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RoleRepository {

    private static transient final Logger LOG = LoggerFactory.getLogger(RoleRepository.class);

    @Autowired
    @PortalDB
    private JdbcTemplate jdbcTemplate;

    public List<RoleDB> getAllRoles() {
        List<RoleDB> roles = jdbcTemplate.query("select * from roles",
                new PortalBeanPropertyRowMapper<>(RoleDB.class));
        return roles;
    }

    public Long save(RoleDB roleDB) {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(roleDB.getJson());
        paramsList.add(roleDB.getCreatedBy());
        paramsList.add(roleDB.getCreationDate());
        paramsList.add(roleDB.getLastUpdatedBy());
        paramsList.add(roleDB.getLastUpdateDate());

        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        StringBuilder sbSql = new StringBuilder("insert into roles (");
        sbSql.append("json,created_by,creation_date,last_updated_by,last_update_date")
                .append(") ")
                .append(JDBCutil.generateValuesPlaceHolderClause(paramArr.length))
                .append(" returning id");

        Long id = jdbcTemplate.queryForObject(sbSql.toString(), paramArr, Long.class);
        roleDB.setId(id);
        return id;
    }

    public void update (RoleDB roleDB) throws JsonProcessingException {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(roleDB.getJson());
        paramsList.add(roleDB.getLastUpdatedBy());
        paramsList.add(roleDB.getLastUpdateDate());
        paramsList.add(roleDB.getId());
        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        int count = jdbcTemplate.update(
                "update roles set json = ?, last_updated_by = ?, last_update_date = ? where id = ? ",
                paramArr);
        LOG.info("Updated " + count + " roles");
    }

    public void delete(Long id) {
        jdbcTemplate.update("delete from roles where id = ?", new Object[]{id});
        LOG.info("Deleted " + id + " from roles");
    }
}
