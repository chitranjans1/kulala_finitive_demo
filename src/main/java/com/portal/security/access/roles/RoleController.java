package com.portal.security.access.roles;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.common.exception.LogException;
import com.portal.common.exception.PortalRuntimeException;
import com.portal.security.access.roles.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController()
@RequestMapping("/rest/priv/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private LogException logException;

    @Autowired
    private ObjectMapper objectMapper;

    @Transactional
    @CrossOrigin
    @RequestMapping(value = "/saveRole", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveRole(@RequestBody String json) throws IOException {
        try {
            Role group = roleService.saveRole(json);
            return new ResponseEntity<>("{\"success\":true, \"id\":" + group.getId() + "}", HttpStatus.CREATED);
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/getAllRoles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllRoles() throws IOException {
        try {
            return objectMapper.writeValueAsString(roleService.getAllRoles());
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/deleteRole/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteDisplayRole(@PathVariable Long id) throws IOException {
        roleService.deleteRole(id);
        return new ResponseEntity<>("{\"success\":true}", HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/getRoleById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getRoleById(@PathVariable Long id) throws IOException {
        try {
            return objectMapper.writeValueAsString(roleService.getRoleById(id));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

}
