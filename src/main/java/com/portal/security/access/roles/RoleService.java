package com.portal.security.access.roles;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.screens.dal.bean.RoleDB;
import com.portal.security.access.roles.domain.Role;
import com.portal.security.access.user.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService {
    private static final Log LOG = LogFactory.getLog(RoleService.class);

    @Autowired
    private RoleCache roleCache;

    @Autowired
    private RoleService roleService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserService userService;

    public Role saveRole(String json) throws IOException {
        Role role = objectMapper.readValue(json, Role.class);

        RoleDB roleDB;
        if( role.getId() == null )
        {
            roleDB = new RoleDB();
            roleDB.setId(role.getId());
            roleDB.setLastUpdateDate(LocalDateTime.now());
            roleDB.setLastUpdatedBy(userService.getUserName());
            roleDB.setJson(json);
            role = roleCache.saveRole(roleDB, role);
        }
        else
        {
            roleDB = new RoleDB();
            roleDB.setId(role.getId());
            roleDB.setLastUpdateDate(LocalDateTime.now());
            roleDB.setLastUpdatedBy(userService.getUserName());
            roleDB.setCreationDate(LocalDateTime.now());
            roleDB.setCreatedBy(userService.getUserName());
            roleDB.setJson(json);
            roleDB.setId(role.getId());
            roleCache.updateRole(roleDB, role);
        }
        return role;
    }

    public List<Role> getAllRoles() {
        return new ArrayList<>(roleCache.getAllRoles());
    }

    public void deleteRole(Long id) throws IOException {
        roleCache.removeRole(id);
    }

    public Role getRoleById(Long id) {
        return roleCache.getRole(id);
    }

}

