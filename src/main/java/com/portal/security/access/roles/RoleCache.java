package com.portal.security.access.roles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.portal.screens.dal.bean.RoleDB;
import com.portal.security.access.roles.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

@Service
public class RoleCache {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private HazelcastInstance hazelcastInstance;

    public static final String GROUP_CACHE = "roleCache";

    public Role saveRole(RoleDB roleDB, Role role) throws JsonProcessingException {
        Long id = roleRepository.save(roleDB);
        role.setId(id);
        String json  = objectMapper.writeValueAsString(role);
        roleDB.setJson(json);
        roleRepository.update(roleDB);
        addToCache(role);
        return role;
    }

    public void updateRole(RoleDB roleDB, Role role) throws JsonProcessingException {
        roleRepository.update(roleDB);
        addToCache(role);
    }

    public void removeRole(Long id) {
        roleRepository.delete(id);
        IMap<Long, Role> cache = hazelcastInstance.getMap(GROUP_CACHE);
        cache.remove(id);
    }

    public Role getRole(Long id) {
        IMap<Long, Role> cache = hazelcastInstance.getMap(GROUP_CACHE);
        return cache.get(id);
    }

    @PostConstruct
    public void loadRoles() throws IOException {
        List<RoleDB> roles = roleRepository.getAllRoles();
        IMap<Long, Role> cache = hazelcastInstance.getMap(GROUP_CACHE);
        for (RoleDB roleDB : roles) {
            Role role = objectMapper.readValue(roleDB.getJson(), Role.class);
            cache.put(role.getId(), role);
        }
    }

    private Role addToCache(Role role) {
        IMap<Long, Role> cache = hazelcastInstance.getMap(GROUP_CACHE);
        cache.put(role.getId(), role);
        return role;
    }

    public Collection<Role> getAllRoles() {
        IMap<Long, Role> cache = hazelcastInstance.getMap(GROUP_CACHE);
        return cache.values();
    }
}
