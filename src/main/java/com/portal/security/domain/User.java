package com.portal.security.domain;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class User {

    private Long userId;
    private String userName;
    private String password;
    private Boolean active;
    private Boolean expired;
    private LocalDateTime creationDate;
    private String createdBy;
    private String lastUpdatedBy;
    private LocalDateTime lastUpdateDate;
}
