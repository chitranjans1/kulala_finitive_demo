package com.portal.security.domain;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UserRegistration {

    private static final Log LOG = LogFactory.getLog(UserRegistration.class);

    private String firstName;
    private String lastName;
    private String emailAddress;
    private String city;
    private String state;
    private String website;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
