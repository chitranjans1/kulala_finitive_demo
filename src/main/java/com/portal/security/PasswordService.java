package com.portal.security;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class PasswordService {

    private static final Log LOG = LogFactory.getLog(PasswordService.class);

    private DefaultPasswordService passwordService;

    @PostConstruct
    private void init() {
        passwordService = new DefaultPasswordService();
    }

    public String encryptPassword(String plainText) {
        return passwordService.encryptPassword(plainText);
    }
}
