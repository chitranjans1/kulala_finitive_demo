package com.portal.screens.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ScreenInstance implements Serializable {
    private Long screenId;
    private Long id;
    private List<ScreenInstancePK> primaryKeys;

}
