package com.portal.screens.domain;

import com.portal.objects.domain.FieldValueObject;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
public class ScreenInstancePK implements Serializable,Comparable<ScreenInstancePK> {

    public static final String NEW_PK = "New";

    private Long objectId;
    private Long relatedId;
    private String name;
    private FieldValueObject pkValue;

    public static ScreenInstancePK newInstance(Long objectId,Long relatedId,String name,FieldValueObject pkValue) {
        ScreenInstancePK instance = new ScreenInstancePK();
        instance.setObjectId(objectId);
        instance.setRelatedId(relatedId);
        instance.setName(name);
        instance.setPkValue(pkValue);
        return instance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScreenInstancePK that = (ScreenInstancePK) o;
        return Objects.equals(objectId, that.objectId) &&
                Objects.equals(relatedId, that.relatedId) &&
                Objects.equals(pkValue, that.pkValue);
    }

    @Override
    public int hashCode() {

        return Objects.hash(objectId, relatedId, pkValue);
    }

    @Override
    public int compareTo(ScreenInstancePK o) {
        int result = objectId.compareTo(o.objectId);
        if( result == 0 )
            result = ObjectUtils.compare(relatedId,o.relatedId);

        return result;
    }
}
