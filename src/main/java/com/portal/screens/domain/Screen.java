package com.portal.screens.domain;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.portal.common.helper.JsonHelper;
import com.portal.displaygroup.domain.DisplayGroup;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Screen implements Serializable {
    private String name;
    private Long id;
    private Long instanceId;

    @JsonFilter(JsonHelper.SCREEN_ID_FILTER)
    private List<Screen> subScreens = new ArrayList<>(1);

    private List<DisplayGroup> displayGroups = new ArrayList<>();
    private String type;

}
