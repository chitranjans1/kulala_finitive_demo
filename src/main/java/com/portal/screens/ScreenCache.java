package com.portal.screens;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.portal.common.util.NumericUtils;
import com.portal.screens.dal.ScreenInstanceRepository;
import com.portal.screens.dal.ScreenRepository;
import com.portal.screens.dal.bean.ScreenDB;
import com.portal.screens.dal.bean.ScreenInstanceDB;
import com.portal.screens.domain.ScreenInstance;
import com.portal.screens.domain.ScreenInstancePK;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class ScreenCache {

    @Autowired
    private ScreenRepository screenRepository;

    @Autowired
    private ScreenInstanceRepository screenInstanceRepository;

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ScreenService screenService;

    public static final String SCREEN_INSTANCE_CACHE = "screenInstanceCache";
    public static final String SCREEN_DEFINITION_CACHE = "screenDefinitionCache";

    public void saveScreen(ScreenDB screenDB) throws JsonProcessingException {
        screenRepository.save(screenDB);
        addToCache(screenDB);
    }

    public void updateScreen(ScreenDB screenDB) throws JsonProcessingException {
        screenRepository.update(screenDB);
        addToCache(screenDB);
    }

    public void removeScreen(Long screenId) {
        screenRepository.delete(screenId);
        getScreenDBCache().remove(screenId);
    }

    public ScreenDB getScreen(Long screenId) {
        return getScreenDBCache().get(screenId);
    }

    public List<ScreenDB> getAllScreens() {
        IMap<Long, ScreenDB> cache = getScreenDBCache();
        Set<Long> keySet = cache.keySet();

        List<ScreenDB> list = new ArrayList<>();
        for (Long key : keySet) {
            list.add(cache.get(key));
        }

        return list;
    }

    public void saveScreenInstance(ScreenInstanceDB screenInstanceDB,ScreenInstance screenInstance) {
        if( screenInstanceDB.getId() != null  )
            screenInstanceRepository.update(screenInstanceDB);
        else
            screenInstanceRepository.save(screenInstanceDB);

        addToCache(screenInstanceDB,screenInstance);
    }

    public void removeScreenInstance(Long screenInstanceId) {
        screenInstanceRepository.delete(screenInstanceId);
        getScreenInstanceDBCache().remove(screenInstanceId);
    }

    public Pair<ScreenInstanceDB,ScreenInstance> getScreenInstancePair(Long screenInstanceId) {
        return getScreenInstanceDBCache().get(screenInstanceId);
    }

    public ScreenInstanceDB getScreenInstanceDB(Long screenInstanceId) {
        Pair<ScreenInstanceDB, ScreenInstance> screenInstancePair = getScreenInstancePair(screenInstanceId);
        return screenInstancePair != null ? screenInstancePair.getKey() : null;
    }

    public ScreenInstance getScreenInstance(Long screenInstanceId) {
        Pair<ScreenInstanceDB, ScreenInstance> screenInstancePair = getScreenInstancePair(screenInstanceId);
        return screenInstancePair != null ? screenInstancePair.getValue() : null;
    }

    public List<ScreenInstance> getAllScreenInstances() {
        IMap<Long, Pair<ScreenInstanceDB, ScreenInstance>> cache = getScreenInstanceDBCache();
        Set<Long> keySet = cache.keySet();

        List<ScreenInstance> list = new ArrayList<>();
        for (Long key : keySet) {
            list.add(cache.get(key).getValue());
        }

        return list;
    }

    @PostConstruct
    public void loadScreenCache() {
        List<ScreenDB> screens = screenRepository.getScreenDBs();
        for (ScreenDB screenDB : screens) {
            addToCache(screenDB);
        }

        List<ScreenInstanceDB> screenInstanceDBs = screenInstanceRepository.getAll();
        for (ScreenInstanceDB screenInstanceDB : screenInstanceDBs) {
            try {
                ScreenInstance screenInstance = objectMapper.readValue(screenInstanceDB.getJson(), ScreenInstance.class);
                addToCache(screenInstanceDB,screenInstance);
            } catch (IOException e) {
                throw new RuntimeException("Unable to parse screen instance json for id "+screenInstanceDB.getId());
            }
        }
    }

    private IMap<Long,ScreenDB> getScreenDBCache() {
        return hazelcastInstance.getMap(SCREEN_DEFINITION_CACHE);
    }

    private void addToCache(ScreenDB screenDB) {
        getScreenDBCache().put(screenDB.getScreenId(), screenDB);
    }

    private IMap<Long,Pair<ScreenInstanceDB,ScreenInstance>> getScreenInstanceDBCache() {
        return hazelcastInstance.getMap(SCREEN_INSTANCE_CACHE);
    }

    private void addToCache(ScreenInstanceDB screenInstanceDB, ScreenInstance screenInstance) {
        screenService.ensurePkValueIsLong(screenInstance);
        getScreenInstanceDBCache().put(screenInstanceDB.getId(), Pair.of(screenInstanceDB,screenInstance));
    }
}
