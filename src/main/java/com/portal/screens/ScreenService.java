package com.portal.screens;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.portal.common.domain.DropdownTitleValue;
import com.portal.common.domain.IdNameBean;
import com.portal.common.domain.grid.Column;
import com.portal.common.domain.grid.Grid;
import com.portal.common.util.NumericUtils;
import com.portal.displaygroup.DisplayGroupService;
import com.portal.objects.ObjectService;
import com.portal.objects.dal.bean.ObjectRelatedValueDB;
import com.portal.objects.domain.Field;
import com.portal.objects.domain.FieldValueObject;
import com.portal.objects.domain.ObjectRelatedValue;
import com.portal.displaygroup.domain.DisplayFieldValue;
import com.portal.displaygroup.domain.DisplayGroup;
import com.portal.screens.dal.bean.ScreenDB;
import com.portal.screens.dal.bean.ScreenInstanceDB;
import com.portal.screens.domain.Screen;
import com.portal.screens.domain.ScreenInstance;
import com.portal.screens.domain.ScreenInstancePK;
import com.portal.common.exception.ValidationException;
import com.portal.common.helper.JsonHelper;
import com.portal.security.access.user.UserService;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ScreenService {
    private static final Log LOG = LogFactory.getLog(ScreenService.class);

    @Autowired
    private ScreenCache screenCache;

    @Autowired
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JsonHelper jsonHelper;

    @Autowired
    private ObjectService objectService;

    @Autowired
    private DisplayGroupService displayGroupService;

    public Screen saveScreen(String json) throws IOException {
        Screen screen = objectMapper.readValue(json, Screen.class);

        if( StringUtils.isBlank(screen.getName()) )
            throw new ValidationException("Please give a name to the screen");

        FilterProvider filterProvider = getFilterProviderForScreenDef();

        ScreenDB screenDB;
        if( screen.getId() != null )
        {
            String updatedJson = objectMapper.writer(filterProvider).writeValueAsString(screen);

            screenDB = new ScreenDB();
            screenDB.setScreenId(screen.getId());
            screenDB.setLastUpdateDate(LocalDateTime.now());
            screenDB.setLastUpdatedBy(userService.getUserName());
            screenDB.setJson(updatedJson);
            screenDB.setName(screen.getName());
            screenCache.updateScreen(screenDB);
        }
        else
        {
            if( getScreenIdsAsList().stream().filter(s -> s.getName().equalsIgnoreCase(screen.getName())).findAny().isPresent() )
                throw new ValidationException("Screen already exists with the same name");

            screenDB = new ScreenDB();
            screenDB.setLastUpdateDate(LocalDateTime.now());
            screenDB.setLastUpdatedBy(userService.getUserName());
            screenDB.setCreationDate(LocalDateTime.now());
            screenDB.setCreatedBy(userService.getUserName());
            screenDB.setJson("{}");
            screenDB.setName(screen.getName());
            screenCache.saveScreen(screenDB);

            screen.setId(screenDB.getScreenId());

            String updatedJson = objectMapper.writer(filterProvider).writeValueAsString(screen);
            screenDB.setJson(updatedJson);
            screenCache.updateScreen(screenDB);
        }

        return screen;
    }

    public FilterProvider getFilterProviderForScreenDef() {
        return jsonHelper.getFilterProvider(
                    jsonHelper.getDisplayFieldDefinitionFilterPair(),
                    jsonHelper.getScreenIdFilterPair()
                    );
    }

    private boolean isScreenPresentInHierarchy(ScreenDB screenDB, Long screenId) throws IOException {
        if(screenId.equals(screenDB.getScreenId()) ) {
            return true;
        }
        ObjectNode jsonNode = (ObjectNode)objectMapper.readTree(screenDB.getJson());
        ArrayNode subScreenIds = (ArrayNode)jsonNode.get("subScreenIds");
        if(subScreenIds != null) {
            for(int i=0;i<subScreenIds.size();i++){
                Long subscreenId =  subScreenIds.get(i).asLong();
                if(screenId.equals(subscreenId)){
                    return true;
                }
                ScreenDB subScreenDB = screenCache.getScreen(subscreenId);
                if(isScreenPresentInHierarchy(subScreenDB, screenId)){
                    return true;
                };
            }
        }
        return false;
    }

    public List<Screen> getAvailableScreens(Long currentScreenId) throws IOException {
        List<Screen> availableScreens = new ArrayList<>();

        List<Long> usedScreenIds = new ArrayList<>();
        usedScreenIds.add(currentScreenId);
        getScreenObjById(currentScreenId).getSubScreens().forEach(s -> usedScreenIds.add(s.getId()));

        List<ScreenDB> screenDBs = screenCache.getAllScreens();
        for (ScreenDB screenDB : screenDBs) {
            if( usedScreenIds.contains(screenDB.getScreenId()) )
                continue;

            Screen screen = getScreenObjById(screenDB.getScreenId());
            List<Screen> subScreens = screen.getSubScreens();
            for (Screen subScreen : subScreens) {
                if( subScreen.getId().equals(currentScreenId) )
                    return Collections.emptyList();
            }

            if( !subScreens.isEmpty() )
                continue;

            availableScreens.add(screen);
        }
        return availableScreens;
    }

    public List<Screen> getSubScreensByParentScreenId(Long parentScreenId) throws IOException {
        return getScreenObjById(parentScreenId).getSubScreens();
    }

    public List<IdNameBean> getScreenIdsAsList() {
        List<ScreenDB> screens = screenCache.getAllScreens();
        return screens.stream().map(o -> IdNameBean.of(o.getScreenId(), o.getName())).collect(Collectors.toList());
    }

    public Screen getScreenObjById(Long screenId) throws IOException {
        ScreenDB screenDB = screenCache.getScreen(screenId);
        Screen screen = objectMapper.readValue(screenDB.getJson(), Screen.class);

        ListIterator<DisplayGroup> listIterator = screen.getDisplayGroups().listIterator();
        while(listIterator.hasNext()) {
            DisplayGroup displayGroup = listIterator.next();
            DisplayGroup newDisplayGroup = displayGroupService.getDisplayGroupById(displayGroup.getId());
            listIterator.set(newDisplayGroup);
        }

        List<Screen> subScreens = screen.getSubScreens();
        if( !subScreens.isEmpty() )
        {
            List<Screen> list = new ArrayList<>();
            for (Screen subScreen : subScreens) {
                list.add(getScreenObjById(subScreen.getId()));
            }
            screen.setSubScreens(list);
        }

        return screen;
    }

    public void deleteScreen(Long screenId) {
        //TODO: check if any screen instances(screen with values) are there

        screenCache.removeScreen(screenId);
    }

    public void ensurePkValueIsLong(ScreenInstance screenInstance) {
        screenInstance.getPrimaryKeys().forEach(screenInstancePK -> ensurePkValueIsLong(screenInstancePK));
    }

    public void ensurePkValueIsLong(ScreenInstancePK pk) {
        FieldValueObject pkValue = pk.getPkValue();
        if( !(pkValue.getValue() instanceof Long) )
            pkValue.setValue(NumericUtils.getNumberAsLong((Number) pkValue.getValue()));
    }

    public Screen getNewScreenInstanceForScreen(Long screenId) throws IOException {
        Screen screen = getScreenObjById(screenId);
        populateDisplayFieldWithFieldObj(screen, null, false);
        return screen;
    }

    public Screen getScreenForScreenInstance(Long screenInstanceId) throws IOException {
        ScreenInstance screenInstance = getScreenInstance(screenInstanceId);
        Screen screen = getScreenObjById(screenInstance.getScreenId());
        screen.setInstanceId(screenInstanceId);
        populateDisplayFieldWithFieldObj(screen, screenInstance, true);
        return screen;
    }

    private void populateDisplayFieldWithFieldObj(Screen screen,ScreenInstance screenInstance,boolean includeValue) throws IOException {
        List<DisplayGroup> displayGroups = screen.getDisplayGroups();
        for (DisplayGroup displayGroup : displayGroups) {
            populateDisplayFieldWithFieldObj(screenInstance,displayGroup,includeValue);
        }

        if( !screen.getSubScreens().isEmpty() )
        {
            List<Screen> subScreens = screen.getSubScreens();
            for (Screen subScreen : subScreens) {
                populateDisplayFieldWithFieldObj(subScreen,screenInstance,includeValue);
            }
        }
    }

    private void populateDisplayFieldWithFieldObj(ScreenInstance screenInstance, DisplayGroup displayGroup, boolean includeValue) throws IOException {
        List<DisplayFieldValue> displayFields = displayGroup.getDisplayFields();
        for (DisplayFieldValue displayField : displayFields) {
            List<Field> objectRelatedFields = objectService.getObjectRelatedFields(displayField.getObjectId(), displayField.getRelatedId());
            Field field = objectRelatedFields.stream().filter(f -> f.getName().equalsIgnoreCase(displayField.getFieldName())).findAny().get();
            displayField.setMeta(field);

            if( Field.PRIMARY_KEY_FIELD.equalsIgnoreCase(field.getType()) ) {
                List<DropdownTitleValue> optionValues = new ArrayList<>();
                optionValues.add(DropdownTitleValue.of("New","New"));

                if( screenInstance != null ) {
                    ScreenInstancePK screenInstancePK = screenInstance.getPrimaryKeys().stream()
                            .filter(pk -> pk.getObjectId().equals(displayField.getObjectId()) && ObjectUtils.equals(pk.getRelatedId(), displayField.getRelatedId()))
                            .findAny().orElse(null);

                    if( screenInstancePK != null ) {
                        Object value = screenInstancePK.getPkValue().getValue();
                        optionValues.add(DropdownTitleValue.of(value.toString(),value));
                    }
                }

//                Grid<Map<String, Object>> screenInstances = getAllScreenInstanceByScreenId(screen.getId());
//                for (Map<String, Object> row : screenInstances.getRows()) {
//                    Long pkValue = (Long) row.get(field.getName());
//                    optionValues.add(DropdownTitleValue.of(pkValue.toString(),pkValue));
//                }

                field.setOptionValues(optionValues);
            }

            if( includeValue ) {
                ScreenInstancePK screenInstancePK = screenInstance.getPrimaryKeys().stream()
                        .filter(pk -> pk.getObjectId().equals(displayField.getObjectId()) && ObjectUtils.equals(pk.getRelatedId(), displayField.getRelatedId()))
                        .findAny().get();

                Long pk = NumericUtils.getNumberAsLong((Number)screenInstancePK.getPkValue().getValue());
                screenInstancePK.getPkValue().setValue(pk);

                ObjectRelatedValue objectRelatedValue = objectService.getObjectRelatedValue(
                        displayField.getObjectId(),
                        displayField.getRelatedId(),
                        pk);
                DisplayFieldValue value = objectRelatedValue.getValue(displayField.getFieldName());
                if( value != null )
                {
                    displayField.setValue(value.getValue());
                }
            }
        }

        List<DisplayGroup> subDisplayGroups = displayGroup.getSubDisplayGroups();
        for (DisplayGroup subDisplayGroup : subDisplayGroups) {
            populateDisplayFieldWithFieldObj(screenInstance, subDisplayGroup, includeValue);
        }
    }

    private String getDisplayFieldValueMapKey(DisplayFieldValue displayFieldValue) {
        return displayFieldValue.getObjectId() + "|" + Objects.toString(displayFieldValue.getRelatedId(),"");
    }

    public DisplayFieldValue getValue(List<DisplayFieldValue> displayFieldValues, String fieldName) {
        return displayFieldValues.stream()
                .filter(v -> v.getFieldName().equalsIgnoreCase(fieldName))
                .findAny()
                .orElse(null);
    }

    public ScreenInstance saveScreenInstance(String json) throws IOException {
        Screen screen = objectMapper.readValue(json, Screen.class);

        List<DisplayFieldValue> allDisplayFieldValues = getAllDisplayFieldValues(screen);
        Map<String, List<DisplayFieldValue>> displayFieldValuesByObject = allDisplayFieldValues.stream()
                .collect(Collectors.groupingBy(dfv -> getDisplayFieldValueMapKey(dfv)));

        SortedSet<ScreenInstancePK> pks = new TreeSet<>();

        for (Map.Entry<String, List<DisplayFieldValue>> entry : displayFieldValuesByObject.entrySet()) {
            String key = entry.getKey();
            String[] keyValuesArr = key.split("\\|");
            Long objectId = Long.parseLong(keyValuesArr[0]);
            Long relatedId = keyValuesArr.length > 1 ? Long.valueOf(keyValuesArr[1]) : null;

            List<DisplayFieldValue> displayFieldValues = entry.getValue();

            ScreenInstancePK pk = displayFieldValues.stream()
                    .filter(v -> Field.PRIMARY_KEY_FIELD.equalsIgnoreCase(v.getMeta().getType()))
                    .map(v -> ScreenInstancePK.newInstance(objectId,relatedId,v.getMeta().getName(),v.getValue()))
                    .findAny().orElse(null);

            if( pk == null )
                throw new ValidationException("Object/Related ("+objectId+","+relatedId+") doesn't have a pk associated");

            pks.add(pk);

            if( !ScreenInstancePK.NEW_PK.equals(pk.getPkValue().getValue()) ) {

                Long pkAsLong = NumericUtils.getNumberAsLong((Number)pk.getPkValue().getValue());
                pk.getPkValue().setValue(pkAsLong);

                Pair<ObjectRelatedValueDB, ObjectRelatedValue> valuePairFromCache = objectService.getObjectRelatedValuePair(
                        objectId, relatedId, pkAsLong);

                ObjectRelatedValueDB objectRelatedValueDB = valuePairFromCache.getKey();
                ObjectRelatedValue objectRelatedValue = valuePairFromCache.getValue();

                //first replace all existing values
                ListIterator<DisplayFieldValue> itr = objectRelatedValue.getDisplayFieldValues().listIterator();
                while(itr.hasNext()) {
                    DisplayFieldValue value = itr.next();
                    DisplayFieldValue newValue = getValue(displayFieldValues,value.getFieldName());
                    if( newValue != null ) {
                        itr.set(newValue);
                        displayFieldValues.remove(newValue);
                    }
                }

                //then add any new ones
                for (DisplayFieldValue displayFieldValue : displayFieldValues) {
                    objectRelatedValue.getDisplayFieldValues().add(displayFieldValue);
                }

                objectService.populateNewSeqs(objectRelatedValue);

                String newValueJson = objectMapper.writeValueAsString(objectRelatedValue);
                objectRelatedValueDB.setJson(newValueJson);
                objectService.saveObjectRelatedValue(objectRelatedValueDB,objectRelatedValue);
            } else {
                ObjectRelatedValue value = new ObjectRelatedValue();
                value.setObjectId(objectId);
                value.setRelatedId(relatedId);
                value.setDisplayFieldValues(new ArrayList<>(displayFieldValues));

                objectService.populateNewSeqs(value);

                ObjectRelatedValueDB db = new ObjectRelatedValueDB();
                db.setJson(objectMapper.writeValueAsString(value));
                objectService.saveObjectRelatedValue(db,value);

            }
        }

        ScreenInstance value;
        if( screen.getInstanceId() != null )
        {
            Pair<ScreenInstanceDB, ScreenInstance> screenInstancePair = screenCache.getScreenInstancePair(screen.getInstanceId());

            value = screenInstancePair.getValue();
            value.setPrimaryKeys(new ArrayList<>(pks));

            ScreenInstanceDB db = screenInstancePair.getKey();
            db.setJson(objectMapper.writeValueAsString(value));
            db.setLastUpdateDate(LocalDateTime.now());
            db.setLastUpdatedBy(userService.getUserName());

            screenCache.saveScreenInstance(db, value);
        }
        else
        {
            value = new ScreenInstance();
            value.setPrimaryKeys(new ArrayList<>(pks));
            value.setScreenId(screen.getId());

            ScreenInstanceDB db = new ScreenInstanceDB();
            db.setLastUpdateDate(LocalDateTime.now());
            db.setLastUpdatedBy(userService.getUserName());
            db.setCreationDate(LocalDateTime.now());
            db.setCreatedBy(userService.getUserName());
            db.setJson("{}");
            screenCache.saveScreenInstance(db,value);

            value.setId(db.getId());

            String updatedJson = objectMapper.writeValueAsString(value);
            db.setJson(updatedJson);
            screenCache.saveScreenInstance(db,value);
        }

        return value;
    }

    private List<DisplayFieldValue> getAllDisplayFieldValues(Screen screen) {

        List<DisplayFieldValue> list = new ArrayList<>();

        List<DisplayGroup> displayGroups = screen.getDisplayGroups();
        for (DisplayGroup displayGroup : displayGroups) {
            list.addAll(displayGroup.getDisplayFields());
            for (DisplayGroup subDisplayGroup : displayGroup.getSubDisplayGroups()) {
                list.addAll(subDisplayGroup.getDisplayFields());
            }
        }

        List<Screen> subScreens = screen.getSubScreens();
        for (Screen subScreen : subScreens) {
            list.addAll(getAllDisplayFieldValues(subScreen));
        }

        return list;
    }

    public ScreenInstance getScreenInstance(Long screenInstanceId) {
        return screenCache.getScreenInstance(screenInstanceId);
    }

    public Grid<Map<String,Object>> getAllScreenInstanceByScreenId(Long screenId) throws IOException {
        Screen screen = getNewScreenInstanceForScreen(screenId);

        List<ScreenInstance> screenInstances = screenCache.getAllScreenInstances().stream()
                .filter(s -> s.getScreenId().equals(screenId))
                .collect(Collectors.toList());

        List<DisplayFieldValue> displayFieldValues = getAllDisplayFieldValues(screen);
        List<Column> columns = new ArrayList<>();
        columns.add(new Column("Id","id"));
        for (DisplayFieldValue displayFieldValue : displayFieldValues) {
            if( Field.PRIMARY_KEY_FIELD.equalsIgnoreCase(displayFieldValue.getMeta().getType()) )
                columns.add(new Column(displayFieldValue.getDisplayName(),displayFieldValue.getMeta().getName()));
        }

        Grid<Map<String,Object>> grid = new Grid();
        grid.setColumns(columns);

        List<Map<String,Object>> rows = new ArrayList<>();
        for (ScreenInstance screenInstance : screenInstances) {
            Map<String,Object> row = new HashMap<>();
            row.put("id",screenInstance.getId());
            for (ScreenInstancePK screenInstancePK : screenInstance.getPrimaryKeys()) {
                row.put(screenInstancePK.getName(),screenInstancePK.getPkValue().getValue());
            }
            rows.add(row);
        }
        grid.setRows(rows);

        return grid;
    }
}
