package com.portal.screens.dal;

import com.portal.common.dal.AbstractIdJsonRepository;
import com.portal.screens.dal.bean.ScreenInstanceDB;
import org.springframework.stereotype.Repository;

@Repository
public class ScreenInstanceRepository extends AbstractIdJsonRepository<ScreenInstanceDB> {
    @Override
    protected String getTableName() {
        return "screen_instances";
    }

    @Override
    protected Class<ScreenInstanceDB> getBeanType() {
        return ScreenInstanceDB.class;
    }
}
