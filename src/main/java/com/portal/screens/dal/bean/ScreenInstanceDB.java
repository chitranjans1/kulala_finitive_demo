package com.portal.screens.dal.bean;

import com.portal.common.dal.GenericIdJsonDB;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScreenInstanceDB extends GenericIdJsonDB {

}
