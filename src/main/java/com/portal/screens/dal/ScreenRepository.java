package com.portal.screens.dal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.portal.common.dal.PortalDB;
import com.portal.screens.dal.bean.ScreenDB;
import com.portal.common.helper.JDBCutil;
import com.portal.common.spring.PortalBeanPropertyRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ScreenRepository {

    private static transient final Logger LOG = LoggerFactory.getLogger(ScreenRepository.class);

    @Autowired
    @PortalDB
    private JdbcTemplate jdbcTemplate;

    public List<ScreenDB> getScreenDBs() {
        List<ScreenDB> objects = jdbcTemplate.query("select * from screens",
                new PortalBeanPropertyRowMapper<>(ScreenDB.class));
        return  objects;
    }

    public Long save(ScreenDB screenDB) throws JsonProcessingException {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(screenDB.getName());
        paramsList.add(screenDB.getJson());
        paramsList.add(screenDB.getCreatedBy());
        paramsList.add(screenDB.getCreationDate());
        paramsList.add(screenDB.getLastUpdatedBy());
        paramsList.add(screenDB.getLastUpdateDate());

        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        StringBuilder sbSql = new StringBuilder("insert into screens (");
        sbSql.append("name,json,created_by,creation_date,last_updated_by,last_update_date")
                .append(") ")
                .append(JDBCutil.generateValuesPlaceHolderClause(paramArr.length))
                .append(" returning screen_id");

        Long id = jdbcTemplate.queryForObject(sbSql.toString(),paramArr,Long.class);
        screenDB.setScreenId(id);
        return id;
    }

    public void update(ScreenDB screenDB) throws JsonProcessingException {
        List<Object> paramsList = new ArrayList<>();
        paramsList.add(screenDB.getName());
        paramsList.add(screenDB.getJson());
        paramsList.add(screenDB.getLastUpdatedBy());
        paramsList.add(screenDB.getLastUpdateDate());
        paramsList.add(screenDB.getScreenId());
        Object[] paramArr = paramsList.toArray();
        JDBCutil.convertLocalDateParams(paramArr);

        int count = jdbcTemplate.update(
                "update screens set name = ?, json = ?, last_updated_by = ?, last_update_date = ? where screen_id = ? ",
                paramArr);
        LOG.info("Updated "+count+" screens");
    }

    public void delete(Long screenId) {
        jdbcTemplate.update("delete from screens where screen_id = ?", new Object[]{screenId});
        LOG.info("Deleted "+screenId+" from screens");
    }
}
