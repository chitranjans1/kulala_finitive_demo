package com.portal.screens;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.portal.screens.domain.Screen;
import com.portal.screens.domain.ScreenInstance;
import com.portal.common.exception.LogException;
import com.portal.common.exception.PortalRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

@RestController
public class ScreenController {

    @Autowired
    private ScreenService screenService;

    @Autowired
    private LogException logException;

    @Autowired
    private ObjectMapper objectMapper;

    @Transactional
    @CrossOrigin
    @RequestMapping(value = "/rest/priv/screen/saveScreen", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveScreen(@RequestBody String json) throws IOException {
        try {
            Screen screen = screenService.saveScreen(json);
            return new ResponseEntity<>("{\"success\":true, \"id\":" + screen.getId() + "}", HttpStatus.CREATED);
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/screen/getScreenById/{screenId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getScreenById(@PathVariable Long screenId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectMapper.writer(screenService.getFilterProviderForScreenDef()).writeValueAsString(screenService.getScreenObjById(screenId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/screen/getAllScreens", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllScreens() throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectMapper.writeValueAsString(screenService.getScreenIdsAsList());
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/screen/getAvailableScreens/{screenId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAvailableScreens(@PathVariable Long screenId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectMapper.writeValueAsString(screenService.getAvailableScreens(screenId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/screen/getSubScreensByParentScreenId/{screenId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getSubScreensByParentScreenId(@PathVariable Long screenId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectMapper.writeValueAsString(screenService.getSubScreensByParentScreenId(screenId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/screen/deleteScreen/{screenId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteScreen(@PathVariable Long screenId) throws IOException {
        screenService.deleteScreen(screenId);
        return new ResponseEntity<>("{\"success\":true}", HttpStatus.OK);

    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/screen/getScreenInstanceById/{screenInstanceId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getScreenInstanceById(@PathVariable Long screenInstanceId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectMapper.writeValueAsString(screenService.getScreenForScreenInstance(screenInstanceId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/screen/getAllScreenInstanceByScreenId/{screenId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllScreenInstanceByScreenId(@PathVariable Long screenId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectMapper.writeValueAsString(screenService.getAllScreenInstanceByScreenId(screenId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/rest/priv/screen/getNewScreenInstanceForScreen/{screenId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getNewScreenInstanceForScreen(@PathVariable Long screenId) throws IOException, InvocationTargetException, IllegalAccessException {
        try {
            return objectMapper.writeValueAsString(screenService.getNewScreenInstanceForScreen(screenId));
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }

    @Transactional
    @CrossOrigin
    @RequestMapping(value = "/rest/priv/screen/saveScreenInstance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveScreenInstance(@RequestBody String json) throws IOException {
        try {
            ScreenInstance screenInstance = screenService.saveScreenInstance(json);
            return new ResponseEntity<>("{\"success\":true, \"id\":" + screenInstance.getId() + "}", HttpStatus.CREATED);
        } catch (Throwable t) {
            logException.log(new PortalRuntimeException(t));
            throw t;
        }
    }
}
