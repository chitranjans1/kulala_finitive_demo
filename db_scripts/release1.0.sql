--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.8
-- Dumped by pg_dump version 9.6.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: screens; Type: TABLE; Schema: public; Owner: portal_tech
--

CREATE TABLE public.screens (
    screen_id bigint NOT NULL,
    name text,
    json text,
    created_by character varying(100),
    creation_date timestamp without time zone,
    last_updated_by character varying(100),
    last_update_date timestamp without time zone
);


ALTER TABLE public.screens OWNER TO portal_tech;

--
-- Name: display_objects_display_object_id_seq; Type: SEQUENCE; Schema: public; Owner: portal_tech
--

CREATE SEQUENCE public.display_objects_display_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.display_objects_display_object_id_seq OWNER TO portal_tech;

--
-- Name: display_objects_display_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: portal_tech
--

ALTER SEQUENCE public.display_objects_display_object_id_seq OWNED BY public.screens.screen_id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: portal_tech
--

CREATE TABLE public.groups (
    group_id bigint NOT NULL,
    group_name text,
    created_by character varying(100),
    creation_date timestamp without time zone,
    last_updated_by character varying(100),
    last_update_date timestamp without time zone
);


ALTER TABLE public.groups OWNER TO portal_tech;

--
-- Name: groups_group_id_seq; Type: SEQUENCE; Schema: public; Owner: portal_tech
--

CREATE SEQUENCE public.groups_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_group_id_seq OWNER TO portal_tech;

--
-- Name: groups_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: portal_tech
--

ALTER SEQUENCE public.groups_group_id_seq OWNED BY public.groups.group_id;


--
-- Name: object_related_values; Type: TABLE; Schema: public; Owner: portal_tech
--

CREATE TABLE public.object_related_values (
    id bigint NOT NULL,
    json text NOT NULL,
    created_by character varying(100),
    creation_date timestamp without time zone,
    last_updated_by character varying(100),
    last_update_date timestamp without time zone
);


ALTER TABLE public.object_related_values OWNER TO portal_tech;

--
-- Name: object_related_values_id_seq; Type: SEQUENCE; Schema: public; Owner: portal_tech
--

CREATE SEQUENCE public.object_related_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.object_related_values_id_seq OWNER TO portal_tech;

--
-- Name: object_related_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: portal_tech
--

ALTER SEQUENCE public.object_related_values_id_seq OWNED BY public.object_related_values.id;


--
-- Name: object_values; Type: TABLE; Schema: public; Owner: portal_tech
--

CREATE TABLE public.object_values (
    id integer,
    name character varying,
    json character varying,
    created_by character varying(100),
    creation_date timestamp without time zone,
    last_updated_by character varying(100),
    last_update_date timestamp without time zone
);


ALTER TABLE public.object_values OWNER TO portal_tech;

--
-- Name: objects; Type: TABLE; Schema: public; Owner: portal_tech
--

CREATE TABLE public.objects (
    id integer NOT NULL,
    name character varying,
    json character varying,
    created_by character varying(100),
    creation_date timestamp without time zone,
    last_updated_by character varying(100),
    last_update_date timestamp without time zone
);


ALTER TABLE public.objects OWNER TO portal_tech;

--
-- Name: objects_object_id_seq; Type: SEQUENCE; Schema: public; Owner: portal_tech
--

CREATE SEQUENCE public.objects_object_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.objects_object_id_seq OWNER TO portal_tech;

--
-- Name: objects_object_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: portal_tech
--

ALTER SEQUENCE public.objects_object_id_seq OWNED BY public.objects.id;


--
-- Name: relateds; Type: TABLE; Schema: public; Owner: portal_tech
--

CREATE TABLE public.relateds (
    id bigint NOT NULL,
    object_id bigint NOT NULL,
    json text,
    created_by character varying(100),
    creation_date timestamp without time zone,
    last_updated_by character varying(100),
    last_update_date timestamp without time zone
);


ALTER TABLE public.relateds OWNER TO portal_tech;

--
-- Name: relateds_id_seq; Type: SEQUENCE; Schema: public; Owner: portal_tech
--

CREATE SEQUENCE public.relateds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relateds_id_seq OWNER TO portal_tech;

--
-- Name: relateds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: portal_tech
--

ALTER SEQUENCE public.relateds_id_seq OWNED BY public.relateds.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: portal_tech
--

CREATE TABLE public.roles (
    role_id bigint NOT NULL,
    role_name text,
    created_by character varying(100),
    creation_date timestamp without time zone,
    last_updated_by character varying(100),
    last_update_date timestamp without time zone
);


ALTER TABLE public.roles OWNER TO portal_tech;

--
-- Name: roles_role_id_seq; Type: SEQUENCE; Schema: public; Owner: portal_tech
--

CREATE SEQUENCE public.roles_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_role_id_seq OWNER TO portal_tech;

--
-- Name: roles_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: portal_tech
--

ALTER SEQUENCE public.roles_role_id_seq OWNED BY public.roles.role_id;


--
-- Name: screen_instances; Type: TABLE; Schema: public; Owner: portal_tech
--

CREATE TABLE public.screen_instances (
    id bigint NOT NULL,
    json text NOT NULL,
    created_by character varying(100),
    creation_date timestamp without time zone,
    last_updated_by character varying(100),
    last_update_date timestamp without time zone
);


ALTER TABLE public.screen_instances OWNER TO portal_tech;

--
-- Name: screen_instances_id_seq; Type: SEQUENCE; Schema: public; Owner: portal_tech
--

CREATE SEQUENCE public.screen_instances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.screen_instances_id_seq OWNER TO portal_tech;

--
-- Name: screen_instances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: portal_tech
--

ALTER SEQUENCE public.screen_instances_id_seq OWNED BY public.screen_instances.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: portal_tech
--

CREATE TABLE public.users (
    user_id integer NOT NULL,
    user_name character varying,
    password character varying,
    active boolean,
    expired boolean,
    created_by character varying(100),
    creation_date timestamp without time zone,
    last_updated_by character varying(100),
    last_update_date timestamp without time zone
);


ALTER TABLE public.users OWNER TO portal_tech;

--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: portal_tech
--

CREATE SEQUENCE public.users_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_id_seq OWNER TO portal_tech;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: portal_tech
--

ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users.user_id;


--
-- Name: groups group_id; Type: DEFAULT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.groups ALTER COLUMN group_id SET DEFAULT nextval('public.groups_group_id_seq'::regclass);


--
-- Name: object_related_values id; Type: DEFAULT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.object_related_values ALTER COLUMN id SET DEFAULT nextval('public.object_related_values_id_seq'::regclass);


--
-- Name: objects id; Type: DEFAULT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.objects ALTER COLUMN id SET DEFAULT nextval('public.objects_object_id_seq'::regclass);


--
-- Name: relateds id; Type: DEFAULT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.relateds ALTER COLUMN id SET DEFAULT nextval('public.relateds_id_seq'::regclass);


--
-- Name: roles role_id; Type: DEFAULT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.roles ALTER COLUMN role_id SET DEFAULT nextval('public.roles_role_id_seq'::regclass);


--
-- Name: screen_instances id; Type: DEFAULT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.screen_instances ALTER COLUMN id SET DEFAULT nextval('public.screen_instances_id_seq'::regclass);


--
-- Name: screens screen_id; Type: DEFAULT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.screens ALTER COLUMN screen_id SET DEFAULT nextval('public.display_objects_display_object_id_seq'::regclass);


--
-- Name: users user_id; Type: DEFAULT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.users ALTER COLUMN user_id SET DEFAULT nextval('public.users_user_id_seq'::regclass);


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: portal_tech
--

COPY public.groups (group_id, group_name, created_by, creation_date, last_updated_by, last_update_date) FROM stdin;
\.


--
-- Data for Name: object_related_values; Type: TABLE DATA; Schema: public; Owner: portal_tech
--

COPY public.object_related_values (id, json, created_by, creation_date, last_updated_by, last_update_date) FROM stdin;
\.


--
-- Data for Name: object_values; Type: TABLE DATA; Schema: public; Owner: portal_tech
--

COPY public.object_values (id, name, json, created_by, creation_date, last_updated_by, last_update_date) FROM stdin;
\.


--
-- Data for Name: objects; Type: TABLE DATA; Schema: public; Owner: portal_tech
--

COPY public.objects (id, name, json, created_by, creation_date, last_updated_by, last_update_date) FROM stdin;
\.


--
-- Data for Name: relateds; Type: TABLE DATA; Schema: public; Owner: portal_tech
--

COPY public.relateds (id, object_id, json, created_by, creation_date, last_updated_by, last_update_date) FROM stdin;
\.

--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: portal_tech
--

COPY public.roles (role_id, role_name, created_by, creation_date, last_updated_by, last_update_date) FROM stdin;
\.


--
-- Data for Name: screen_instances; Type: TABLE DATA; Schema: public; Owner: portal_tech
--

COPY public.screen_instances (id, json, created_by, creation_date, last_updated_by, last_update_date) FROM stdin;
\.

--
-- Data for Name: screens; Type: TABLE DATA; Schema: public; Owner: portal_tech
--

COPY public.screens (screen_id, name, json, created_by, creation_date, last_updated_by, last_update_date) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: portal_tech
--

COPY public.users (user_id, user_name, password, active, expired, created_by, creation_date, last_updated_by, last_update_date) FROM stdin;
\.


--
-- Name: screens display_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.screens
    ADD CONSTRAINT display_objects_pkey PRIMARY KEY (screen_id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (group_id);


--
-- Name: object_related_values pk_object_related_values; Type: CONSTRAINT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.object_related_values
    ADD CONSTRAINT pk_object_related_values PRIMARY KEY (id);


--
-- Name: objects pk_objects; Type: CONSTRAINT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.objects
    ADD CONSTRAINT pk_objects PRIMARY KEY (id);


--
-- Name: relateds pk_relateds; Type: CONSTRAINT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.relateds
    ADD CONSTRAINT pk_relateds PRIMARY KEY (id);


--
-- Name: screen_instances pk_screen_instances; Type: CONSTRAINT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.screen_instances
    ADD CONSTRAINT pk_screen_instances PRIMARY KEY (id);


--
-- Name: users pk_users; Type: CONSTRAINT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT pk_users PRIMARY KEY (user_id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: portal_tech
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (role_id);


--
-- PostgreSQL database dump complete
--

